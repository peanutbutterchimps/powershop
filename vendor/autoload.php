<?php
require __DIR__ .'/../WooCommerce/Client.php';
require __DIR__ .'/../WooCommerce/HttpClient/Response.php';
require __DIR__ .'/../WooCommerce/HttpClient/Request.php';
require __DIR__ .'/../WooCommerce/HttpClient/Options.php';
require __DIR__ .'/../WooCommerce/HttpClient/OAuth.php';
require __DIR__ .'/../WooCommerce/HttpClient/HttpClientException.php';
require __DIR__ .'/../WooCommerce/HttpClient/HttpClient.php';
require __DIR__ .'/../WooCommerce/HttpClient/BasicAuth.php';
//require 'WooCommerce/HttpClient/HttpClient.php';
?>
