<?php
/**
 * File to load generated classes once at once time
 * @package TC
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-09
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-09
 */
require_once dirname(__FILE__) . '/TCWsdlClass.php';
require_once dirname(__FILE__) . '/WS/TCServiceWS.php';
require_once dirname(__FILE__) . '/TCClassMap.php';
