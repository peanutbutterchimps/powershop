<?php
/**
 * File for class TCServiceWS
 * @package TC
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-09
 */
/**
 * This class stands for TCServiceWS originally named WS
 * @package TC
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2016-12-09
 */
class TCServiceWS extends TCWsdlClass
{
    /**
     * Method to call the operation originally named WS_ConfirmarStock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_ConfirmarStock($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarStock($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConfirmarClienteMayor
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @return array
     */
    public function WS_ConfirmarClienteMayor($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarClienteMayor($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConfirmarPreferencias
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @return void
     */
    public function WS_ConfirmarPreferencias($_entrada1,$_entrada2,$_entrada3,$_entrada4)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarPreferencias($_entrada1,$_entrada2,$_entrada3,$_entrada4));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_Stock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_FF_Stock($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_Stock($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_Ventas
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @param string $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param string $_entrada21
     * @param string $_entrada22
     * @param string $_entrada23
     * @param string $_entrada24
     * @param boolean $_entrada25
     * @return array
     */
    public function WS_FF_Ventas($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_Ventas($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConfirmarStock_Resto
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_ConfirmarStock_Resto($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarStock_Resto($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarCabecerasPedidos
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param boolean $_entrada5
     * @param date $_entrada6
     * @param date $_entrada7
     * @param date $_entrada8
     * @param date $_entrada9
     * @param date $_entrada10
     * @param date $_entrada11
     * @return array
     */
    public function WS_BuscarCabecerasPedidos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarCabecerasPedidos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetallePedido
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetallePedido($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetallePedido($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarCabecerasAlbaranes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param boolean $_entrada6
     * @param boolean $_entrada7
     * @param boolean $_entrada8
     * @param boolean $_entrada9
     * @param date $_entrada10
     * @param date $_entrada11
     * @return array
     */
    public function WS_BuscarCabecerasAlbaranes($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarCabecerasAlbaranes($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetalleAlbaran
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetalleAlbaran($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetalleAlbaran($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarCabecerasFacturas
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param boolean $_entrada6
     * @param boolean $_entrada7
     * @param date $_entrada8
     * @param date $_entrada9
     * @param date $_entrada10
     * @param date $_entrada11
     * @return array
     */
    public function WS_BuscarCabecerasFacturas($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarCabecerasFacturas($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetalleFactura
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetalleFactura($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetalleFactura($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_InformacionStock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @return array
     */
    public function WS_InformacionStock($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_InformacionStock($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConsultarTesoreria
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @param boolean $_entrada4
     * @param date $_entrada5
     * @param date $_entrada6
     * @return array
     */
    public function WS_ConsultarTesoreria($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConsultarTesoreria($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PedidoCommerce
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @param string $_entrada4
     * @param date $_entrada5
     * @param date $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @param string $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param string $_entrada21
     * @param string $_entrada22
     * @param string $_entrada23
     * @param string $_entrada24
     * @param string $_entrada25
     * @param string $_entrada26
     * @param float $_entrada27
     * @param string $_entrada28
     * @param string $_entrada29
     * @param string $_entrada30
     * @param string $_entrada31
     * @param string $_entrada32
     * @return string
     */
    public function WS_PedidoCommerce($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PedidoCommerce($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConfirmarStockArray
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_ConfirmarStockArray($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarStockArray($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarClientesPorRepres
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @return array
     */
    public function WS_BuscarClientesPorRepres($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarClientesPorRepres($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarContactosPorCliente
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @param float $_entrada4
     * @return array
     */
    public function WS_BuscarContactosPorCliente($_entrada1,$_entrada2,$_entrada3,$_entrada4)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarContactosPorCliente($_entrada1,$_entrada2,$_entrada3,$_entrada4));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetallePedidoSTAN
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetallePedidoSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetallePedidoSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetalleAlbaranSTAN
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetalleAlbaranSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetalleAlbaranSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetalleFacturaSTAN
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @return array
     */
    public function WS_BuscarDetalleFacturaSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetalleFacturaSTAN($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ConfirmarPagoOnline
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @return string
     */
    public function WS_ConfirmarPagoOnline($_entrada1,$_entrada2,$_entrada3,$_entrada4)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ConfirmarPagoOnline($_entrada1,$_entrada2,$_entrada3,$_entrada4));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_log_user
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param boolean $_entrada3
     * @param boolean $_entrada4
     * @return array
     */
    public function WS_PWC_log_user($_entrada1,$_entrada2,$_entrada3,$_entrada4)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_log_user($_entrada1,$_entrada2,$_entrada3,$_entrada4));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_update_user
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @param string $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param string $_entrada21
     * @param boolean $_entrada22
     * @param string $_entrada23
     * @param string $_entrada24
     * @return string
     */
    public function WS_PWC_update_user($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_update_user($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_Clientes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_FF_Clientes($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_Clientes($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_BorrarVenta
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_string
     * @return string
     */
    public function WS_FF_BorrarVenta($_string)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_BorrarVenta($_string));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_WS_Stock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @return array
     */
    public function WS_WS_Stock($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_WS_Stock($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ClienteMultiChannel
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param boolean $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param float $_entrada10
     * @param float $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param int $_entrada21
     * @param int $_entrada22
     * @param float $_entrada23
     * @param float $_entrada24
     * @param float $_entrada25
     * @param boolean $_entrada26
     * @param boolean $_entrada27
     * @param boolean $_entrada28
     * @param string $_entrada29
     * @param string $_entrada30
     * @param string $_entrada31
     * @param string $_entrada32
     * @param string $_entrada33
     * @param string $_entrada34
     * @param string $_entrada35
     * @param string $_entrada36
     * @param string $_entrada37
     * @param string $_entrada38
     * @param string $_entrada39
     * @param string $_entrada40
     * @param string $_entrada41
     * @param string $_entrada42
     * @param string $_entrada43
     * @param string $_entrada44
     * @param string $_entrada45
     * @param string $_entrada46
     * @param string $_entrada47
     * @param string $_entrada48
     * @param string $_entrada49
     * @param string $_entrada50
     * @param string $_entrada51
     * @param string $_entrada52
     * @param string $_entrada53
     * @param string $_entrada54
     * @param string $_entrada55
     * @param string $_entrada56
     * @param string $_entrada57
     * @param date $_entrada58
     * @param boolean $_entrada59
     * @param string $_entrada60
     * @param string $_entrada61
     * @param int $_entrada62
     * @return array
     */
    public function WS_ClienteMultiChannel($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32,$_entrada33,$_entrada34,$_entrada35,$_entrada36,$_entrada37,$_entrada38,$_entrada39,$_entrada40,$_entrada41,$_entrada42,$_entrada43,$_entrada44,$_entrada45,$_entrada46,$_entrada47,$_entrada48,$_entrada49,$_entrada50,$_entrada51,$_entrada52,$_entrada53,$_entrada54,$_entrada55,$_entrada56,$_entrada57,$_entrada58,$_entrada59,$_entrada60,$_entrada61,$_entrada62)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ClienteMultiChannel($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32,$_entrada33,$_entrada34,$_entrada35,$_entrada36,$_entrada37,$_entrada38,$_entrada39,$_entrada40,$_entrada41,$_entrada42,$_entrada43,$_entrada44,$_entrada45,$_entrada46,$_entrada47,$_entrada48,$_entrada49,$_entrada50,$_entrada51,$_entrada52,$_entrada53,$_entrada54,$_entrada55,$_entrada56,$_entrada57,$_entrada58,$_entrada59,$_entrada60,$_entrada61,$_entrada62));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_ContactoMultiChannel
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @return string
     */
    public function WS_ContactoMultiChannel($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_ContactoMultiChannel($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_VentaMultiChannelSDC
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param float $_entrada10
     * @param string $_entrada11
     * @param boolean $_entrada12
     * @param string $_entrada13
     * @param date $_entrada14
     * @param date $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param int $_entrada21
     * @param string $_entrada22
     * @param boolean $_entrada23
     * @param boolean $_entrada24
     * @param boolean $_entrada25
     * @param int $_entrada26
     * @return array
     */
    public function WS_VentaMultiChannelSDC($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_VentaMultiChannelSDC($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDatosContacto
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @return array
     */
    public function WS_BuscarDatosContacto($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDatosContacto($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarContactos
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @return array
     */
    public function WS_BuscarContactos($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarContactos($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_check_email
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param int $_entrada3
     * @return array
     */
    public function WS_PWC_check_email($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_check_email($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_login
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param boolean $_entrada4
     * @param boolean $_entrada5
     * @param int $_entrada6
     * @return array
     */
    public function WS_PWC_login($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_login($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_save_contacto
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @param string $_entrada11
     * @return string
     */
    public function WS_PWC_save_contacto($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_save_contacto($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_update_cliente
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param string $_entrada10
     * @param string $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param string $_entrada21
     * @param string $_entrada22
     * @param date $_entrada23
     * @param boolean $_entrada24
     * @param string $_entrada25
     * @return string
     */
    public function WS_PWC_update_cliente($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_update_cliente($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_get_password
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param int $_entrada3
     * @return array
     */
    public function WS_PWC_get_password($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_get_password($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetallePedido2
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @param int $_entrada6
     * @return array
     */
    public function WS_BuscarDetallePedido2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetallePedido2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarDetalleFactura2
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param float $_entrada3
     * @param int $_entrada4
     * @param int $_entrada5
     * @param int $_entrada6
     * @return array
     */
    public function WS_BuscarDetalleFactura2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarDetalleFactura2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarCabecerasFacturas2
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param boolean $_entrada6
     * @param boolean $_entrada7
     * @param date $_entrada8
     * @param date $_entrada9
     * @param date $_entrada10
     * @param date $_entrada11
     * @param int $_entrada12
     * @return array
     */
    public function WS_BuscarCabecerasFacturas2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarCabecerasFacturas2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_BuscarCabecerasPedidos2
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param boolean $_entrada5
     * @param date $_entrada6
     * @param date $_entrada7
     * @param date $_entrada8
     * @param date $_entrada9
     * @param date $_entrada10
     * @param date $_entrada11
     * @param int $_entrada12
     * @return array
     */
    public function WS_BuscarCabecerasPedidos2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_BuscarCabecerasPedidos2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_Interface_Pedidos
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param int $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param float $_entrada10
     * @param boolean $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param float $_entrada16
     * @param float $_entrada17
     * @param float $_entrada18
     * @param boolean $_entrada19
     * @return array
     */
    public function WS_Interface_Pedidos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_Interface_Pedidos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_Interface_Albaranes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param date $_entrada1
     * @return array
     */
    public function WS_Interface_Albaranes($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_Interface_Albaranes($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_Interface_Traspasos
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @return array
     */
    public function WS_Interface_Traspasos($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_Interface_Traspasos($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_Interface_Clientes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_Interface_Clientes($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_Interface_Clientes($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_update_cliente_field
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param float $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @return string
     */
    public function WS_PWC_update_cliente_field($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_update_cliente_field($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_ResumenVentas
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param date $_entrada2
     * @param date $_entrada3
     * @return array
     */
    public function WS_FF_ResumenVentas($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_ResumenVentas($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_FF_ActualizaCampos
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @return string
     */
    public function WS_FF_ActualizaCampos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_FF_ActualizaCampos($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_PWC_login2
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param boolean $_entrada4
     * @param boolean $_entrada5
     * @param int $_entrada6
     * @return array
     */
    public function WS_PWC_login2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_PWC_login2($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_SeleccionStock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_TO_SeleccionStock($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_SeleccionStock($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_DesgloseStock
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_TO_DesgloseStock($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_DesgloseStock($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_Forum_ConsultaPrecios
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @return array
     */
    public function WS_Forum_ConsultaPrecios($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_Forum_ConsultaPrecios($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_CalculoPortes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param float $_entrada2
     * @param boolean $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @return array
     */
    public function WS_TO_CalculoPortes($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_CalculoPortes($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_ClienteFidelizacion
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param int $_entrada2
     * @param float $_entrada3
     * @param float $_entrada4
     * @param boolean $_entrada5
     * @param float $_entrada6
     * @param float $_salida1
     * @param float $_salida2
     * @return array
     */
    public function WS_TO_ClienteFidelizacion($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_salida1,$_salida2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_ClienteFidelizacion($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_salida1,$_salida2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_SeleccionStockModelo
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @return array
     */
    public function WS_TO_SeleccionStockModelo($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_SeleccionStockModelo($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_BuscarClientes
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @return float
     */
    public function WS_TO_BuscarClientes($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_BuscarClientes($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_BuscarFacturas
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @return array
     */
    public function WS_TO_BuscarFacturas($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_BuscarFacturas($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_GrabarPedidoCliente
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param float $_entrada1
     * @param boolean $_entrada2
     * @param int $_entrada3
     * @param string $_entrada4
     * @param string $_entrada5
     * @param string $_entrada6
     * @param string $_entrada7
     * @param string $_entrada8
     * @param string $_entrada9
     * @param boolean $_entrada10
     * @param string $_entrada11
     * @param string $_entrada12
     * @param string $_entrada13
     * @param string $_entrada14
     * @param string $_entrada15
     * @param string $_entrada16
     * @param string $_entrada17
     * @param string $_entrada18
     * @param string $_entrada19
     * @param string $_entrada20
     * @param string $_entrada21
     * @param string $_entrada22
     * @param string $_entrada23
     * @param string $_entrada24
     * @param string $_entrada25
     * @param string $_entrada26
     * @param string $_entrada27
     * @param string $_entrada28
     * @param string $_entrada29
     * @param string $_entrada30
     * @param string $_entrada31
     * @param string $_entrada32
     * @param string $_entrada33
     * @param string $_entrada34
     * @param string $_entrada35
     * @param string $_entrada36
     * @param string $_entrada37
     * @param string $_entrada38
     * @param string $_entrada39
     * @param string $_entrada40
     * @param string $_entrada41
     * @param string $_entrada42
     * @param string $_entrada43
     * @param string $_entrada44
     * @param string $_entrada45
     * @param string $_entrada46
     * @param string $_entrada47
     * @param string $_entrada48
     * @param date $_entrada49
     * @param boolean $_entrada50
     * @param string $_entrada51
     * @param string $_entrada52
     * @param string $_entrada53
     * @param string $_entrada54
     * @param string $_entrada55
     * @param string $_entrada56
     * @param boolean $_entrada57
     * @param boolean $_entrada58
     * @param string $_entrada59
     * @param string $_entrada60
     * @param string $_entrada61
     * @param string $_entrada62
     * @param string $_entrada63
     * @return array
     */
    public function WS_TO_GrabarPedidoCliente($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32,$_entrada33,$_entrada34,$_entrada35,$_entrada36,$_entrada37,$_entrada38,$_entrada39,$_entrada40,$_entrada41,$_entrada42,$_entrada43,$_entrada44,$_entrada45,$_entrada46,$_entrada47,$_entrada48,$_entrada49,$_entrada50,$_entrada51,$_entrada52,$_entrada53,$_entrada54,$_entrada55,$_entrada56,$_entrada57,$_entrada58,$_entrada59,$_entrada60,$_entrada61,$_entrada62,$_entrada63)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_GrabarPedidoCliente($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7,$_entrada8,$_entrada9,$_entrada10,$_entrada11,$_entrada12,$_entrada13,$_entrada14,$_entrada15,$_entrada16,$_entrada17,$_entrada18,$_entrada19,$_entrada20,$_entrada21,$_entrada22,$_entrada23,$_entrada24,$_entrada25,$_entrada26,$_entrada27,$_entrada28,$_entrada29,$_entrada30,$_entrada31,$_entrada32,$_entrada33,$_entrada34,$_entrada35,$_entrada36,$_entrada37,$_entrada38,$_entrada39,$_entrada40,$_entrada41,$_entrada42,$_entrada43,$_entrada44,$_entrada45,$_entrada46,$_entrada47,$_entrada48,$_entrada49,$_entrada50,$_entrada51,$_entrada52,$_entrada53,$_entrada54,$_entrada55,$_entrada56,$_entrada57,$_entrada58,$_entrada59,$_entrada60,$_entrada61,$_entrada62,$_entrada63));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TestConexion
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param int $_entrada1
     * @return array
     */
    public function WS_TestConexion($_entrada1)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TestConexion($_entrada1));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TestConexionRecs
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param int $_entrada1
     * @param int $_entrada2
     * @param boolean $_entrada3
     * @return array
     */
    public function WS_TestConexionRecs($_entrada1,$_entrada2,$_entrada3)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TestConexionRecs($_entrada1,$_entrada2,$_entrada3));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_RecogeInventarioRFID
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param string $_entrada2
     * @param string $_entrada3
     * @param string $_entrada4
     * @param date $_entrada5
     * @param time $_entrada6
     * @param string $_entrada7
     * @return string
     */
    public function WS_RecogeInventarioRFID($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_RecogeInventarioRFID($_entrada1,$_entrada2,$_entrada3,$_entrada4,$_entrada5,$_entrada6,$_entrada7));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named WS_TO_SeleccionStockExtranjero
     * Documentation : no documentation is available
     * @uses TCWsdlClass::getSoapClient()
     * @uses TCWsdlClass::setResult()
     * @uses TCWsdlClass::saveLastError()
     * @param string $_entrada1
     * @param int $_entrada2
     * @return array
     */
    public function WS_TO_SeleccionStockExtranjero($_entrada1,$_entrada2)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->WS_TO_SeleccionStockExtranjero($_entrada1,$_entrada2));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see TCWsdlClass::getResult()
     * @return array|float|string|void
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
