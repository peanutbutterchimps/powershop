=== Plugin Name ===
Contributors: pablogiralt
Donate link: http://peanutbutter.es
Tags: woocommerce, powershop, stock, inventory management, erp
Requires at least: 4.7.0
Tested up to: 4.9.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Powershop ERP and Woocommerce integration.

== Description ==

Integra Woocommerce con el ERP Powershop

Estas son las rpincipales funcionalidades:

*   Importación de productos desde Powershop
*   Sincronización de stock desde Powershop
*   Envío de pedidos desde Woocommerce a Powershop

== Installation ==

Para instalar el plugin:

1. Upload `powershop` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Ve a powershop/opciones y configura el plugin

== Changelog ==

= 1.0.10 =
* Arreglos para hacerlo compatible con última versión de Woocommerce

= 1.0.9 =
* AHora se ejecutan las acciones añadidas después de la importación antes de ejecutar la reindexaxión de Facetwp debido a que esta a veces no terminaba

= 1.0.7 =
* Añadido archivo Readme.txt

= 1.0.6 =
* Añadido comprobación de actualizaciones y actualización automática