<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://peanutbutter.es
 * @since      1.0.0
 *
 * @package    Powershop
 * @subpackage Powershop/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
