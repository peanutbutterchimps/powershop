<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://peanutbutter.es
 * @since             1.0.0
 * @package           Powershop
 *
 * @wordpress-plugin
 * Plugin Name:       Powershop
 * Plugin URI:        http://peanutbutter.es
 * Description:       Powershop ERP and Woocommerce integration.
 * Version:           1.0.10
 * Author:            Pablo Giralt
 * Author URI:        http://peanutbutter.es
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       powershop
 * Domain Path:       /languages
 */

// Plugin generated with https://wppb.me/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.10' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-powershop-activator.php
 */
function activate_powershop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-powershop-activator.php';
	Powershop_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-powershop-deactivator.php
 */
function deactivate_powershop() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-powershop-deactivator.php';
	Powershop_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_powershop' );
register_deactivation_hook( __FILE__, 'deactivate_powershop' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-powershop.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_powershop() {

	$plugin = new Powershop(WP_PLUGIN_DIR.'/powershop');
	$plugin->run();

	return $plugin;

}

/**
 * Check if WooCommerce is active
 **/
function checkIfWoocommerce(){
	return in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
}

if (checkIfWoocommerce()) {
	run_powershop();
}else{
	//@todo Añadir admin notive si woocommerce no esta activo - mirar como lo hace el plugin de stripe que tambien mira la version mínima.
}

/*
 * Check for updates
 */ 
if ( is_admin() ) {
	require plugin_dir_path( __FILE__ ) . 'includes/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'http://updates.peanutbutter.es/?action=get_metadata&slug=powershop',
		__FILE__, //Full path to the main plugin file or functions.php.
		'powershop'
	);
}



