<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://peanutbutter.es
 * @since      1.0.1
 *
 * @package    Powershop
 * @subpackage Powershop/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Powershop
 * @subpackage Powershop/admin
 * @author     Pablo Giralt <pablo@peanutbutter.es>
 */
class Powershop_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $path;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $dirpath = WP_PLUGIN_DIR.'/powershop' ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->path = $dirpath;
	}

	public function get_path(){
		return $this->path;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Powershop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Powershop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/powershop-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts($hook) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Powershop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Powershop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

	  if($hook == 'powershop_page_powershop_import'||$hook == 'powershop_page_powershop_logs') {

			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/powershop-admin.js', array( 'jquery' ), $this->version, false );
			wp_localize_script( $this->plugin_name, $this->plugin_name, array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

		}

	}

	public function create_admin_page(){
		add_menu_page( 'Powershop', 'Powershop', 'edit_posts', 'powershop', function(){
			echo "<h1>".$this->plugin_name."</h1>";
		}, '', 20 );

		add_submenu_page( 'powershop', 'Importar Productos', 'Importar Productos', 'edit_posts', 'powershop_import', function(){
			$this->admin_page_powershop_import();
		} );

		add_submenu_page( 'powershop', 'Opciones', 'Opciones', 'manage_options', 'powershop_settings', function(){
			$this->admin_page_powershop_settings();
		} );

		add_submenu_page( 'powershop', 'Logs', 'Logs', 'manage_options', 'powershop_logs', function(){
			$this->admin_page_powershop_logs();
		} );

		add_submenu_page( 'powershop', 'Test Stock', 'Test Stock', 'edit_posts', 'powershop_test_stock', function(){
			$this->admin_page_powershop_test_stock();
		} );

		add_action( 'admin_init', function(){
			// Datos de conexión con BBDD de Powershop
			register_setting( 'powershop-settings-group', 'powershop_db_host' );
			register_setting( 'powershop-settings-group', 'powershop_db_name' );
			register_setting( 'powershop-settings-group', 'powershop_db_user' );
			register_setting( 'powershop-settings-group', 'powershop_db_pass' );

			// Datos de conexión con los Webservices de Powershop
			register_setting( 'powershop-settings-group', 'powershop_wsdl_url' );

			// Otras opciones de Powershop
			register_setting( 'powershop-settings-group', 'powershop_talla_unica' );

			// Datos de conexión con Rest API de Woocommerce
			register_setting( 'powershop-settings-group', 'powershop_woo_ck' );
			register_setting( 'powershop-settings-group', 'powershop_woo_cs' );

			// Columnas en Powershop Usadas para la importación
			register_setting( 'powershop-settings-group', 'powershop_column_name' );
			register_setting( 'powershop-settings-group', 'powershop_column_description' );
			register_setting( 'powershop-settings-group', 'powershop_column_price' );
			register_setting( 'powershop-settings-group', 'powershop_column_category_parent' );
			register_setting( 'powershop-settings-group', 'powershop_column_category_child' );

			// Atributos de Woocommerce usados para Marca y Tallas
			register_setting( 'powershop-settings-group', 'powershop_attribute_brand' );
			register_setting( 'powershop-settings-group', 'powershop_attribute_sizes' );
			register_setting( 'powershop-settings-group', 'powershop_attribute_color' );

			// Trabajos Cron
			register_setting( 'powershop-settings-group', 'powershop_shop_url' );
			register_setting( 'powershop-settings-group', 'powershop-cron-stock-active' );
			register_setting( 'powershop-settings-group', 'powershop-cron-stock-mins' );
			register_setting( 'powershop-settings-group', 'powershop-cron-orders-active' );
			register_setting( 'powershop-settings-group', 'powershop-cron-orders-mins' );
			register_setting( 'powershop-settings-group', 'powershop-cron-orders-limit' );

			// Notificaciones
			register_setting( 'powershop-settings-group', 'powershop-notifications-emails' );

			// Otros settings
			register_setting( 'powershop-settings-group', 'powershop_import_status' );

			// Testing settings
			register_setting( 'powershop-settings-group', 'powershop_test_active' );
			register_setting( 'powershop-settings-group', 'powershop_test_codes' );
		} );
	}


	public function admin_page_powershop_test_stock(){

		$max_skus = 20;

		if(isset($_POST['skus'])){
			$skus = str_ireplace(' ', '', $_POST['skus']);
		}else{
			$skus = '';
		}

		?>
			<div class="wrap">
				<h1><?= __('Testear Stock', 'powershop').' '.$this->plugin_name; ?></h1>

				<p><?php printf(__('Para testear el stock añade un sku por linea en el siguiente campo.  Puedes añadir un máximo de %s skus', 'powershop'), $max_skus);?></p>
				<p><?= __('Si hay algún sku para el que no se devuelve el stock, lo más probable es que ese código no exista en Powershop', 'powershop'); ?></p>
				<form method="post">
					<textarea name="skus" rows="10" cols="8"><?= $skus; ?></textarea>
					<p class="submit">
						<input type="submit" name="submit" id="submit" class="button button-primary" value="Enviar Códigos">
					</p>
				</form>
			</div>

		<?php

		if(isset($_POST['skus'])){

			// split string by line breaks
			$skus_array = preg_split('/\r\n|[\r\n]/', $skus);

			// If we have SKUs test the stock for them
			if(!empty($skus_array)){
				$this->powershopTestStock($skus_array, $max_skus);
			}
		}
	}

	private function powershopTestStock($skus, $max_skus){

		if(count($skus) > $max_skus){
			$skus = array_slice ($skus, 0, $max_skus);
		}

		$skus_string = implode('|', $skus);

		if($skus_string){

			try{
				require_once($this->get_path().'/webservices/TCAutoload.php');
				$wsdl = $this->initPowershopService();
				$tCServiceWS = new TCServiceWS($wsdl);

				$productStocksResponse = $this->powershopGetStock($tCServiceWS, $skus_string);

				if($productStocksResponse['status'] == 'ok'){
					if(count($productStocksResponse['response'])){

						?>
							<style>
								#stock-table td, #stock-table th{
									padding: 5px 20px;
									border: 2px solid transparent;
								}
								#stock-table th {
									background-color: #0085ba;
									color: #fff;
								}
								#stock-table td {
									background-color: #fff;
								}
							</style>

						<?php

						echo '<table id="stock-table">';
						echo '<tr>';
						echo '<th>'.__('sku', 'powershop').'</th>';
						echo '<th>'.__('talla', 'powershop').'</th>';
						echo '<th>'.__('stock', 'powershop').'</th>';
						echo '</tr>';
						foreach ($productStocksResponse['response'] as $sku => $product){

							echo '<tr>';
							echo '<td>'.$sku.'</td><td></td><td></td>';
							echo '<tr>';

							if(count($product)){
								foreach ($product as $size => $stock){
									echo '<tr>';
										echo '<td></td>';
										echo '<td>'.$size.'</td>';
										echo '<td>'.$stock.'</td>';
									echo '<tr>';
								}
							}

							echo '<tr>';
							echo '<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
							echo '<tr>';
						}
						echo '</table>';
					}else{
						echo __('Powershop no ha devuelto stock para ningún producto.  Puede ser que los códigos introducidos no estén disponibles en Powershop','powershop');
					}
				}else{
					// @todo - gestionar y explicar error
					echo __('Ha habido un error conectando con Powershop','powershop');
				}


			}catch(Exception $e){
				echo __('Ha habido un error conectando con Powershop:','powershop');
				echo '<br>';
				echo $e->getMessage();

				return;
			}


		}

		//echo $skus_string;
	}


	public function admin_page_powershop_settings(){
		?>
		<div class="wrap">
			<h1><?= $this->plugin_name; ?></h1>

			<form method="post" action="options.php">
		    <?php settings_fields( 'powershop-settings-group' ); ?>
		    <?php do_settings_sections( 'powershop-settings-group' ); ?>

				<h2><?= __('Datos de conexión con BBDD de Powershop', 'powershop');?></h2>
		    <table class="form-table">
			    <tr valign="top">
		        <th scope="row"><?= __('Host de la BBDD', 'powershop');?></th>
		        <td><input type="text" name="powershop_db_host" value="<?php echo esc_attr( get_option('powershop_db_host') ); ?>" /></td>
		      </tr>

			    <tr valign="top">
		        <th scope="row"><?= __('Nombre de la BBDD', 'powershop');?></th>
		        <td><input type="text" name="powershop_db_name" value="<?php echo esc_attr( get_option('powershop_db_name') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Usuario de la BBDD', 'powershop');?></th>
		        <td><input type="text" name="powershop_db_user" value="<?php echo esc_attr( get_option('powershop_db_user') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Contraseña de la BBDD', 'powershop');?></th>
		        <td><input type="text" name="powershop_db_pass" value="<?php echo esc_attr( get_option('powershop_db_pass') ); ?>" /></td>
		      </tr>
			  </table>

			  <h2><?= __('Datos de conexión con los Webservices de Powershop', 'powershop');?></h2>
			  <table class="form-table">
					<tr valign="top">
		        <th scope="row"><?= __('URL de los Webservices de Powershop', 'powershop');?></th>
		        <td><input type="text" name="powershop_wsdl_url" value="<?php echo esc_attr( get_option('powershop_wsdl_url') ); ?>" /></td>
		      </tr>
				</table>

				<h2><?= __('Otras opciones de Powershop', 'powershop');?></h2>
			  <table class="form-table">
					<tr valign="top">
		        <th scope="row"><?= __('Talla usada para los productos con talla única (generalmente algo como U o Única', 'powershop');?></th>
		        <td><input type="text" name="powershop_talla_unica" value="<?php echo esc_attr( get_option('powershop_talla_unica') ); ?>" /></td>
		      </tr>
				</table>

			  <h2><?= __('Datos de conexión con Rest API de Woocommerce', 'powershop');?></h2>
			  <table class="form-table">
					<tr valign="top">
		        <th scope="row"><?= __('Consumer Key', 'powershop');?></th>
		        <td><input type="text" name="powershop_woo_ck" value="<?php echo esc_attr( get_option('powershop_woo_ck') ); ?>" /></td>
		      </tr>
		      <tr valign="top">
		        <th scope="row"><?= __('Consumer Secret', 'powershop');?></th>
		        <td><input type="text" name="powershop_woo_cs" value="<?php echo esc_attr( get_option('powershop_woo_cs') ); ?>" /></td>
		      </tr>
			  </table>

		    <h2><?= __('Columnas en Powershop Usadas para la importación', 'powershop');?></h2>
		    <table class="form-table">

			    <tr valign="top">
		        <th scope="row"><?= __('Usada para el nombre del producto', 'powershop');?></th>
		        <td><input type="text" name="powershop_column_name" value="<?php echo esc_attr( get_option('powershop_column_name') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Usada para la descripción del producto', 'powershop');?></th>
		        <td><input type="text" name="powershop_column_description" value="<?php echo esc_attr( get_option('powershop_column_description') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Usada para el precio del producto', 'powershop');?></th>
		        <td><input type="text" name="powershop_column_price" value="<?php echo esc_attr( get_option('powershop_column_price') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Usada para la categoría Padre', 'powershop');?></th>
		        <td><input type="text" name="powershop_column_category_parent" value="<?php echo esc_attr( get_option('powershop_column_category_parent') ); ?>" /></td>
		      </tr>

		      <tr valign="top">
		        <th scope="row"><?= __('Usada para la categoría Hija', 'powershop');?></th>
		        <td><input type="text" name="powershop_column_category_child" value="<?php echo esc_attr( get_option('powershop_column_category_child') ); ?>" /></td>
		      </tr>

		    </table>

				<h2><?= __('Atributos de Woocommerce usados para Marca, Tallas y Color', 'powershop');?></h2>

		    <table class="form-table">

			    <?php $attributes = wc_get_attribute_taxonomies(); ?>

			    <tr valign="top">
				  	<th scope="row"><?= __('Usado para la marca', 'powershop');?></th>
				  	<td>
					  	<select name="powershop_attribute_brand">
						  	<option><?= __('Selecciona un atributo', 'powershop'); ?></option>
						  	<?php if(!empty($attributes)){ ?>
							  	<?php foreach ($attributes as $attribute){ ?>
										<option value="<?= $attribute->attribute_id; ?>" <?= esc_attr(get_option('powershop_attribute_brand')) == $attribute->attribute_id ? 'selected':''  ?>>
											<?= $attribute->attribute_name; ?>
										</option>
							  	<?php } ?>
						  	<?php } ?>
					  	</select>
				  	</td>
			    </tr>

			    <tr valign="top">
				  	<th scope="row"><?= __('Usado para las tallas', 'powershop');?></th>
				  	<td>
					  	<select name="powershop_attribute_sizes">
						  	<option><?= __('Selecciona un atributo', 'powershop'); ?></option>
						  	<?php if(!empty($attributes)){ ?>
							  	<?php foreach ($attributes as $attribute){ ?>
										<option value="<?= $attribute->attribute_id; ?>" <?= esc_attr(get_option('powershop_attribute_sizes')) == $attribute->attribute_id ? 'selected':''  ?>>
											<?= $attribute->attribute_name; ?>
										</option>
							  	<?php } ?>
						  	<?php } ?>
					  	</select>
				  	</td>
			    </tr>

			    <tr valign="top">
				  	<th scope="row"><?= __('Usado para el color', 'powershop');?></th>
				  	<td>
					  	<select name="powershop_attribute_color">
						  	<option><?= __('Selecciona un atributo', 'powershop'); ?></option>
						  	<?php if(!empty($attributes)){ ?>
							  	<?php foreach ($attributes as $attribute){ ?>
										<option value="<?= $attribute->attribute_id; ?>" <?= esc_attr(get_option('powershop_attribute_color')) == $attribute->attribute_id ? 'selected':''  ?>>
											<?= $attribute->attribute_name; ?>
										</option>
							  	<?php } ?>
						  	<?php } ?>
					  	</select>
				  	</td>
			    </tr>

		    </table>

		    <h2><?= __('Trabajos Cron', 'powershop');?></h2>
		    <table class="form-table">
			    <tr valign="top">
		        <th scope="row"><?= __('URL de la tienda online', 'powershop');?></th>
		        <td><input type="text" name="powershop_shop_url" value="<?php echo esc_attr( get_option('powershop_shop_url') ); ?>" /></td>
		      </tr>
			    <tr valign="top">
		        <th scope="row"><?= __('Activar sync de Stock', 'powershop');?></th>
		        <td>
			        <input type="checkbox" name="powershop-cron-stock-active" value="1" <?php checked(1,  esc_attr(get_option('powershop-cron-stock-active')), true); ?> />
			        <?= __('Activar sync de Stock', 'powershop');?>
			        <br>
			      </td>
		      </tr>
			  	<tr valign="top">
		        <th scope="row"><?= __('Cada cuanto se ejecuta la sincro de stock (en minutos)', 'powershop');?></th>
		        <td><input type="number" min="1" max="1440" name="powershop-cron-stock-mins" value="<?php echo esc_attr( get_option('powershop-cron-stock-mins') ); ?>" /></td>
		      </tr>
		      <tr valign="top">
		        <th scope="row"><?= __('Activar sync de Pedidos', 'powershop');?></th>
		        <td>
			        <input type="checkbox" name="powershop-cron-orders-active" value="1" <?php checked(1,  esc_attr(get_option('powershop-cron-orders-active')), true); ?> />
			        <?= __('Activar sync de Pedidos', 'powershop');?>
			        <br>
			      </td>
		      </tr>
		      <tr valign="top">
		        <th scope="row"><?= __('Cada cuanto se ejecuta la sincro de pedidos (en minutos)', 'powershop');?></th>
		        <td><input type="number" min="1" max="1440" name="powershop-cron-orders-mins" value="<?php echo esc_attr( get_option('powershop-cron-orders-mins') ); ?>" /></td>
		      </tr>
		      <tr valign="top">
		        <th scope="row"><?= __('Cual es el máximo de pedidos procesados en cada trabajo cron', 'powershop');?></th>
		        <td><input type="number" min="1" max="100" name="powershop-cron-orders-limit" value="<?php echo esc_attr( get_option('powershop-cron-orders-limit') ); ?>" /></td>
		      </tr>
			  </table>


			  <h2><?= __('Notificaciones', 'powershop');?></h2>
		    <table class="form-table">
			  	<tr valign="top">
		        <th scope="row"><?= __('Emails a los que enviar las notificaciones (separados por comas)', 'powershop');?></th>
		        <td><input type="text" name="powershop-notifications-emails" value="<?php echo esc_attr( get_option('powershop-notifications-emails') ); ?>" /></td>
		      </tr>
			  </table>

		    <h2><?= __('Otras opciones', 'powershop');?></h2>

		    <table class="form-table">

		    	<?php $statuses = get_post_statuses(); ?>

		    	<tr valign="top">
				  	<th scope="row"><?= __('Estado de los productos después de la importación', 'powershop');?></th>
				  	<td>
					  	<select name="powershop_import_status">
						  	<?php if(!empty($attributes)){ ?>
							  	<option><?= __('Selecciona un estado', 'powershop'); ?></option>
							  	<?php foreach ($statuses as $status => $label){ ?>
										<option value="<?= $status; ?>" <?= esc_attr(get_option('powershop_import_status')) == $status ? 'selected':''  ?>>
											<?= $label; ?>
										</option>
							  	<?php } ?>
						  	<?php } ?>
					  	</select>
				  	</td>
			    </tr>

		    </table>

		    <h2><?= __('Opciones de Testing', 'powershop');?></h2>

		    <table class="form-table">

		    	<tr valign="top">
				  	<th scope="row"><?= __('Activar modo de test', 'powershop');?></th>
				  	<td>
							<input type="checkbox" name="powershop_test_active" value="1" <?php checked(1,  esc_attr(get_option('powershop_test_active')), true); ?> />
			        <?= __('Activar modo de test', 'powershop');?>
			        <br>
				  	</td>
				  	<tr valign="top">
			        <th scope="row"><?= __('Códigos para testing (separados por comas)', 'powershop');?></th>
			        <td><input type="text" name="powershop_test_codes" value="<?php echo esc_attr( get_option('powershop_test_codes') ); ?>" /></td>
			      </tr>
			    </tr>

		    </table>

		    <?php do_action('powershop_settings_after'); ?>

		    <?php submit_button(); ?>

			</form>
		</div>
		<?php
	}

	public function admin_page_powershop_logs(){
		?>
		<div class="wrap">
			<h1><?= $this->plugin_name; ?> Logs</h1>

			<?php
				$limit = 50;

				$where_clause = '';
				$where = [];
				if($_GET['log_context']){
					$where[] = ' context="'.$_GET['log_context'].'"';
				}
				if($_GET['log_type']){
					$where[] = ' type="'.$_GET['log_type'].'"';
				}
				if($_GET['log_message']){
					$where[] = ' message LIKE "%'.$_GET['log_message'].'%"';
				}
				if($_GET['log_identifier']){
					$where[] = ' identifier="'.$_GET['log_identifier'].'"';
				}
				if(!empty($where)){
					$where_clause = 'WHERE '.implode(' AND ', $where);
				}

				$total = $this->getLogsCount($limit, $where_clause);

				$pages = ceil($total/$limit);
				if($_GET['paged']>1){
					$offset = ($_GET['paged']-1) * $limit;
				}else{
					$offset = 0;
				}

				$logs = $this->getLogs($limit, $offset, $where_clause);

			?>

			<div class="tablenav top">
				<button id="powershop-search-logs" class="button button-primary">Filtrar</button>
				<div class="tablenav-pages">
					<span class="displaying-num"><?= $total; ?> elementos</span>
					<span class="pagination-links">

						<?php if($_GET['paged']-1<1){ ?>
							<span class="tablenav-pages-navspan" aria-hidden="true">«</span>
							<span class="tablenav-pages-navspan">‹</span>
						<?php }else{ ?>
							<a class="next-page" href="/wp-admin/admin.php?page=powershop_logs&amp;paged=1">
								<span class="screen-reader-text">Primera Página</span><span aria-hidden="true">«</span>
							</a>
							<a class="next-page" href="/wp-admin/admin.php?page=powershop_logs&amp;paged=<?= $_GET['paged']-1 ?>">
								<span class="screen-reader-text">Página Anterior</span><span aria-hidden="true">‹</span>
							</a>
						<?php } ?>
						<span class="paging-input">
							<label for="current-page-selector" class="screen-reader-text">Página actual</label>
							<input class="current-page" id="current-page-selector" type="text" name="paged" value="<?= $_GET['paged']; ?>" size="2" aria-describedby="table-paging">
							<span class="tablenav-paging-text"> de <span class="total-pages"><?= $pages; ?></span></span>
						</span>

						<?php if($_GET['paged']<$pages){ ?>
							<a class="next-page" href="/wp-admin/admin.php?page=powershop_logs&amp;paged=<?= $_GET['paged']+1 ?>">
								<span class="screen-reader-text">Página siguiente</span><span aria-hidden="true">›</span>
							</a>
							<a class="last-page" href="/wp-admin/admin.php?page=powershop_logs&amp;paged=<?= $pages; ?>">
								<span class="screen-reader-text">Última página</span><span aria-hidden="true">»</span>
							</a>
						<?php }else{ ?>
							<span class="tablenav-pages-navspan" aria-hidden="true">›</span>
							<span class="tablenav-pages-navspan">»</span>
						<?php } ?>
					</span>
				</div>
			</div>

			<table class="wp-list-table widefat fixed striped posts">
				<thead>
					<tr>
						<th>
							<p><?= __('Tipo', 'powershop');?></p>
							<select id="powershop-logs-type" name="type">
								<option value></option>
								<option value="notification" <?= isset($_GET['log_type'])&&$_GET['log_type']=='notification'?'selected':''; ?>>
									<?= __('Notificación', 'powershop'); ?>
								</option>
								<option value="error" <?= isset($_GET['log_type'])&&$_GET['log_type']=='error'?'selected':''; ?>>
									<?= __('Error', 'powershop'); ?>
								</option>
							</select>
						</th>
						<th>
							<p><?= __('Contexto', 'powershop');?></p>
							<select id="powershop-logs-context" name="context">
								<option value></option>
								<option value="syncStock" <?= isset($_GET['log_context'])&&$_GET['log_context']=='syncStock'?'selected':''; ?>>
									<?= __('Sync de Stock', 'powershop'); ?>
								</option>
								<option value="syncOrders" <?= isset($_GET['log_context'])&&$_GET['log_context']=='syncOrders'?'selected':''; ?>>
									<?= __('Sync de Pedidos', 'powershop'); ?>
								</option>
								<option value="PS Connection" <?= isset($_GET['log_context'])&&$_GET['log_context']=='PS Connection'?'selected':''; ?>>
									<?= __('Conexión con PS', 'powershop'); ?>
								</option>
							</select>
						</th>
						<th>
							<p><?= __('Mensaje', 'powershop');?></p>
							<input id="powershop-logs-msg" type="text" name="message" value="<?= isset($_GET['log_message'])?$_GET['log_message']:''; ?>">
						</th>
						<th>
							<p><?= __('Identificador', 'powershop');?></p>
							<input id="powershop-logs-identifier" type="text" name="identifier" value="<?= isset($_GET['log_identifier'])?$_GET['log_identifier']:''; ?>">
						</th>
						<th>
							<p><?= __('Fecha', 'powershop');?></p>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($logs)){ ?>
						<?php foreach($logs as $log){ ?>
							<tr>
								<td><?= $log->type; ?></td>
								<td><?= $log->context; ?></td>
								<td><?= $log->message; ?></td>
								<td><?= $log->identifier; ?></td>
								<td><?= $log->time; ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<?php
	}

	private function getLogs($limit, $offset, $where_clause){
		global $wpdb;
		return $wpdb->get_results(
			"SELECT * FROM {$wpdb->prefix}powershop_logs $where_clause ORDER BY id DESC ".($limit ? 'LIMIT '.$limit : '')." OFFSET $offset"
		);
	}


	private function getLogsCount($limit, $where_clause){
		global $wpdb;

		$result = $wpdb->get_results(
			"SELECT COUNT(id) FROM {$wpdb->prefix}powershop_logs $where_clause"
		);

		return array_map('reset', $result)[0];
	}

	public function admin_page_powershop_import(){

		$limit = null;

		// Get the plugin setting and check for errors
		$settingsCheck = $this->getPluginSettings();

		// Display errors
		if(!empty($settingsCheck['errors'])){
			echo '<h2>'.__('Hay errores en las opciones del plugin y no se puede realizar la importación:', 'powershop').'</h2>';
			echo '<ul>';
			foreach ($settingsCheck['errors'] as $error){
				echo '<li>'.$error.'</li>';
			}
			echo '</ul>';

			echo '<a href="/wp-admin/admin.php?page=powershop_settings">'.__('Revisa las opciones del plugin', 'powershop').'</a>';

			return;
		}

		// Display warnings
		if(!empty($settingsCheck['warnings'])){
			echo '<p><strong>'.__('Hay avisos sobre tus opciones del plugin pero nada que no permita la importación:').'</strong></p>';
			echo '<ul>';
			foreach ($settingsCheck['warnings'] as $warning){
				echo '<li>'.$warning.'</li>';
			}
			echo '</ul>';
		}

		$settings = $settingsCheck['settings'];

		// Get connection to Powershop database
		$connection = $this->getPowershopDBConnection();

		// Check connection to database for errors
		if($connection['status'] == 'error'){
			echo '<h2 style="color:red;">'.__("No se puede conectar a Powershop.  Estos son los detalles del error:").'<h2>';
			//@todo show errors with more style
			echo "<pre>";
				print_r($connection);
			echo "</pre>";
			return;
		}

		// Get the columns of the PRODUCTOS table in Powershop
		$powershopColumns = $this->getPowershopColumns($connection['conn'], $settings['db_name']);
		if(empty($powershopColumns)){
			echo '<h2 style="color:red;">'.__("Powershop no esta devolviendo columnas para la tabla de productos.  No se puede importar.").'<h2>';
			return;
		}

		// Check that the powershop columns defined in the plugin setting exist in Powershop
		$powershopColumnErrors = $this->powershopColumnErrors($powershopColumns, $settings);
		if(!empty($powershopColumnErrors)){
			echo '<p style="color:red;">'.__("Hay columnas de Powershop definidas en las opciones del plugin que no existen en la BBDD de Powershop:").'<p>';
			echo '<ul>';
				foreach ($powershopColumnErrors as $error){
					echo '<li>'.$error.'</li>';
				}
			echo '</ul>';
			echo '<a href="/wp-admin/admin.php?page=powershop_settings">'.__('Revisa las opciones del plugin', 'powershop').'</a>';
			return;
		}

		// Get all Powershop products
		$powershop_products = $this->getPowershopProducts($connection['conn'], $settings, $limit);

		// Get all Woocommerce products
		$woo_products = $this->GetAllWooProducts($limit);

		// Get new Powershop products
		$new_products = array_diff_key($powershop_products, $woo_products);

		?>

		<div id="powershop-import-products-content">

			<?php if(count($new_products)){ ?>
				<p class="product-count" data-count="<?= count($new_products); ?>">
					<?= sprintf(__('Hay %s productos nuevos','powershop'),'<span>'.count($new_products).'</span>'); ?>
				</p>

				<!-- Print JSON with new Powershop products to make available to js script -->
				<script type="application/json" id="new-powershop-products">
					<?= $this->safe_json_encode($new_products); ?>
				</script>

				<!-- @todo - mover estilos a hoja de estilos -->
				<style>
					.progress-bar {
						height: 40px;
						width: 100%;
						background-color: #fff;
						color:#fff;
						line-height: 40px;
						font-size: 18px;
						margin-bottom: 40px;
					}
					.progress-bar-inner {
						height: 40px;
						width: 0%;
						background-color: #4CAF50;
						text-align: center;
						transition: 200ms width linear;
					}
					#import-log {
						background-color: #fff;
						padding: 20px;
						margin-top: 40px;
					}
					#import-log #import-log-inner li.error {
						color: red;
					}
					#import-log #import-log-inner li.good {
						color: #4CAF50;
					}
				</style>

				<div class="progress-bar">
					<div class="progress-bar-inner"></div>
				</div>

				<p class="import-messages"></p>

				<button id="import-products-btn" class="button button-primary"><?= __('Importar Productos'); ?></button>

				<div id="import-log">
					<ul id="import-log-inner">

					</ul>
				</div>

			<?php }else{ ?>
				<p><?= __('No hay productos nuevos para importar','powershop'); ?></p>
			<?php } ?>

		</div>

		<?php

	}

	private function powershopColumnErrors($powershopColumns, $settings){

		$checkColumns = [ 'name', 'description', 'price', 'parent_cat', 'child_cat' ];

		$errors = [];

		foreach ($checkColumns as $column){
			if($settings[$column]!=='' && !isset($powershopColumns[$settings[$column]])){
				$errors[] = sprintf(__('La columna %s no existe en Powershop', 'powershop'), '<strong>'.$settings[$column].'</strong>');
			}
		}

		return $errors;

	}

	private function getPowershopColumns($powershopConnection, $db_name){
		try{
			$tables_query = $powershopConnection->prepare(
				"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$db_name."' AND TABLE_NAME = 'PRODUCTOS';"
			);
			$tables_query->execute();
			return array_map('reset', $tables_query->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC));

		}catch(PDOException $e){
			echo "<pre>";
				print_r($e->getCode());
				print_r($e->getMessage());
			echo "</pre>";
			return [];
		}
	}

	private function getPluginSettings(){
		$settings = [
			'name' 					=> esc_attr(get_option('powershop_column_name')),
			'description' 	=> esc_attr(get_option('powershop_column_description')),
			'price' 				=> esc_attr(get_option('powershop_column_price')),
			'parent_cat' 		=> esc_attr(get_option('powershop_column_category_parent')),
			'child_cat' 		=> esc_attr(get_option('powershop_column_category_child')),
			'brand_attr_id' => esc_attr(get_option('powershop_attribute_brand')),
			'sizes_attr_id' => esc_attr(get_option('powershop_attribute_sizes')),
			'color_attr_id' => esc_attr(get_option('powershop_attribute_color')),
			'import_status' => esc_attr(get_option('powershop_import_status')),

			'db_host' => esc_attr(get_option('powershop_db_host')),
			'db_name' => esc_attr(get_option('powershop_db_name')),
			'db_user' => esc_attr(get_option('powershop_db_user')),
			'db_pass' => esc_attr(get_option('powershop_db_pass')),

			'woo_ck' => esc_attr(get_option('powershop_woo_ck')),
			'woo_cs' => esc_attr(get_option('powershop_woo_cs')),
		];

		$required = [
			'name' => sprintf(__('No has definido una columna de Powershop para %s de los productos', 'powershop'), __('el Nombre', 'powershop')),
			'price' => sprintf(__('No has definido una columna de Powershop para %s de los producto', 'powershop'), __('el Precio', 'powershop')),
			'sizes_attr_id' => __('No has definido un attributo de Woocommerce para las tallas de los productos', 'powershop'),
			'db_host' => sprintf(__('No has definido %s para la conexión a la BBDD de Powershop', 'powershop'), __('el Host', 'powershop')),
			'db_name' => sprintf(__('No has definido %s para la conexión a la BBDD de Powershop', 'powershop'), __('el Nombre', 'powershop')),
			'db_user' => sprintf(__('No has definido %s para la conexión a la BBDD de Powershop', 'powershop'), __('el Usuario', 'powershop')),
			'db_pass' => sprintf(__('No has definido %s para la conexión a la BBDD de Powershop', 'powershop'), __('la Contraseña', 'powershop')),
			'woo_ck' => sprintf(__('No has definido %s para la conexión a la REST API de Woocommerce', 'powershop'), __('el Consumer Key', 'powershop')),
			'woo_cs' => sprintf(__('No has definido %s para la conexión a la REST API de Woocommerce', 'powershop'), __('el Consumer Secret', 'powershop')),
		];

		$requiredInteger = ['sizes_attr_id', 'brand_attr_id'];

		$warningsMessages = [
			'parent_cat' => __('No has definido Una columna en powershop para la categoría padre. No se importaran dos niveles de categorías', 'powershop'),
			'child_cat'=> __('No has definido Una columna en powershop para la categoría hija. No se importaran las categorías', 'powershop'),
			'brand_attr_id'=> __('No has definido un atributo de Woocommerce para la marca. No se importaran las marcas', 'powershop'),
			'color_attr_id'=> __('No has definido un atributo de Woocommerce los colores. No se importaran los colores', 'powershop'),
			'import_status'=> __('No has definido un estado para la importación.  Por defecto se importara en estado borrador', 'powershop'),
		];

		$errors = [];
		$warnings = [];

		foreach ($settings as $settingName => $settingValue ){

			if(isset($required[$settingName]) && ($settingValue == '' || (in_array($settingName, $requiredInteger) && (string)(int)$settingValue != $settingValue))){
				$errors[] = $required[$settingName];
				continue;
			}

			if(isset($warningsMessages[$settingName]) && ($settingValue == '' || (in_array($settingName, $requiredInteger) && (string)(int)$settingValue != $settingValue))){
				$warnings[] = $warningsMessages[$settingName];
				continue;
			}
		}

		return [
			'errors' => $errors,
			'warnings' => $warnings,
			'settings' => $settings,
		];
	}

	private function safe_json_encode($value, $options = 0, $depth = 512){
    $encoded = json_encode($value, $options, $depth);
    switch (json_last_error()) {
	    case JSON_ERROR_NONE:
	        return $encoded;
	    case JSON_ERROR_DEPTH:
	        return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
	    case JSON_ERROR_STATE_MISMATCH:
	        return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
	    case JSON_ERROR_CTRL_CHAR:
	        return 'Unexpected control character found';
	    case JSON_ERROR_SYNTAX:
	        return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
	    case JSON_ERROR_UTF8:
	        $clean = $this->utf8ize($value);
	        return $this->safe_json_encode($clean, $options, $depth);
	    default:
	        return 'Unknown error'; // or trigger_error() or throw new Exception()

    }
	}

	private function utf8ize($mixed) {
	    if (is_array($mixed)) {
	        foreach ($mixed as $key => $value) {
	            $mixed[$key] = $this->utf8ize($value);
	        }
	    } else if (is_string ($mixed)) {
	        return utf8_encode($mixed);
	    }
	    return $mixed;
	}

	public function ajax_import_powershop_product(){

		header('Content-Type: application/json');

		$product = $_POST['product'];

		$import_this_product = apply_filters( 'powershop_product_import_this', true, $product );

		if(!$import_this_product){
			echo json_encode([
				'status'=>'error',
				'error'=>[
					'type'=>__('Se ha filtrado este producto para no importarlo', 'powershop'),
				]
			]);
			wp_die();
		}

		require $this->path.'/vendor/autoload.php';

		$wooClient = new Automattic\WooCommerce\Client(
			site_url(),
			esc_attr(get_option('powershop_woo_ck')),
			esc_attr(get_option('powershop_woo_cs')),
			[
		 		'wp_api' => true,
		 		'version' => 'wc/v1',
		 		'query_string_auth' => true,
		 	]
		);

		$name = '';
		// Get Name and check if not black
		if($product['name'] !== ''){
			$name = $product['name'];
			//@todo meter setting para ver si hay que capitalizar el título
			if(true){
				$name = $this->capitalizar($name);
			}
		}
		$name =  apply_filters( 'powershop_product_import_name', $name, $product );

		if($name == ''){
			echo json_encode([
				'status'=>'error',
				'error'=>[
					'type'=>__('El producto no tiene Nombre', 'powershop'),
					'error'=>$error
				]
			]);
			wp_die();
		}

		$description = '';
		if(isset($_POST['description'])){
			$description = $_POST['description'];
		}
		$description =  apply_filters( 'powershop_product_import_description', $description, $product );

		// Get SKU and check if not black
		if($_POST['sku'] !== ''){
			$sku = $_POST['sku'];
		}else{
			echo json_encode([
				'status'=>'error',
				'error'=>[
					'type'=>__('El producto no tiene Código/SKU', 'powershop'),
					'error'=>$error
				]
			]);
			wp_die();
		}

		// Get category IDs
		$categoryIds = $this->createCategoriesIfNeeded($product);

		// Get import post status
		$status = esc_attr(get_option('powershop_import_status'));
		$status = $status ? $status : 'draft';

		// Basic product data
		$data = [
		 'name' => $name,
		 'sku' => $sku,
		 'status' => $status,
		];

		if($description != ''){
			$data['description'] = $description;
		}

		$data['attributes'] = [];

		// Product brand
		if(isset($product['Marca']) && $product['Marca'] !== ''){
			$data['attributes'][] = [
		 		'id' => esc_attr(get_option('powershop_attribute_brand')),
		 		'position' => 1,
		 		'visible' => true,
		 		'variation' => false,
		 		'options' => [$this->capitalizar_palabras($product['Marca']) ]
			];
		}

		// Product color
		// Check if has color and color does not have numbers
		if(isset($product['Color']) && $product['Color'] !== '' && 1 !== preg_match('~[0-9]~', $product['Color'])){
			$data['attributes'][] = [
		 		'id' => esc_attr(get_option('powershop_attribute_color')),
		 		'position' => 5,
		 		'visible' => true,
		 		'variation' => false,
		 		//'options' => ['Melocotón']
		 		'options' => [ $this->capitalizar_palabras($product['Color']) ]
			];
		}

		// Product categories
		$product_categories = array();
		if($categoryIds['parent']){
			$product_categories[] = [
				'id' => $categoryIds['parent']
			];
		}
		if($categoryIds['child']){
			$product_categories[] = [
				'id' => $categoryIds['child']
			];
		}
		if(!empty($product_categories)){
			$data['categories']=$product_categories;
		}

		// Product Sizes
		$tallas = explode('|', $product['Tallas']);
		$tallas = array_filter($tallas, function($value) { return $value !== ''; });

		// Prevent duplicate sizes
		$tallas = array_unique($tallas);

		// Get Id of Woocommerce attribute used for the sizes
		$sizes_attribute = esc_attr(get_option('powershop_attribute_sizes'));

		// Get talla UNICA name
		$talla_unica_name = esc_attr( get_option('powershop_talla_unica'));

		if (count($tallas) == 1) {

			if(!$talla_unica_name){
				echo json_encode([
				'status'=>'error',
					'error'=>[
						'type'=>__('El nombre de la talla única de Powershop no esta definido en las opciones', 'powershop')
					]
				]);
				wp_die();
			}

			if($tallas[0] !== $talla_unica_name){
				echo json_encode([
				'status'=>'error',
					'error'=>[
						'type'=>__('El nombre de la talla única en Powershop no concuerda con el definido en las opciones', 'powershop')
					]
				]);
				wp_die();
			}

			// Simple product
			$data['type'] = 'simple';
			$data['regular_price'] = $product['price'];
			$data['tax_status'] = 'taxable';
			$data['stock'] = 0;
			$data['stock_quantity'] = 0;
			$data['manage_stock'] = true;
		}else{
			// Variable product

			// Remove talla "UNICA"
			$search_unica = array_search($talla_unica_name, $tallas);
			if($search_unica !== FALSE){
				unset($tallas[$search_unica]);
			}

			$data['type'] = 'variable';
			$variaciones = [];
			array_push(
				$data['attributes'],
				[
					'id' => $sizes_attribute,
					'position' => 0,
					'visible' => true,
					'variation' => true,
					'options' => $tallas
				]
			);
			foreach($tallas as $talla) {
				// Quitamos tallas vacias y la talla 'UNICA'
				if ($talla != '' && $talla != $talla_unica_name) {
					array_push($variaciones,
						[
							'regular_price' => $product['price'],
							'attributes' => array(
								['tax_status' => 'taxable'],
								[
									'id' => $sizes_attribute,
									'option' => $talla
								]
							),
							'stock' => 0,
							'manage_stock' => true,
							'stock_quantity' => 0
						]
					);
				}
			}
			$data['variations'] = $variaciones;
		}

		$data = apply_filters( 'powershop_product_import_data', $data, $product );

		if(!empty($data)){
			try{
				echo json_encode([
					'status'=>'ok',
					'product'=>$wooClient->post('products', $data)
				]);
				wp_die();
			} catch ( Exception $e ) {

				$error = [
					'msg' => $e->getMessage(),
					'code' => $e->getCode(),
				];

				echo json_encode([
					'status'=>'error',
					'error'=>[
						'type'=>__('Error al importar el producto', 'powershop'),
						'error'=>$error
					]
				]);
				wp_die();
			}
		}else{
			//@todo - mostrar mensaje de producto no importado
			echo json_encode([
				'status'=>'ok'
			]);
			wp_die();
		}


		wp_die(); // this is required to terminate immediately and return a proper response

	}

	public function ajax_after_import_powershop_products(){

		header('Content-Type: application/json');

		// Add other actions here
		do_action('after_import_powershop_products');

		// We reindex FacetWP index
		$this->reindexFacetWP();

		echo json_encode(['status'=>'ok']);

		wp_die();
	}

	//private function get

	private function capitalizar($string){
		return ucfirst(mb_strtolower($string));
	}

	private function capitalizar_palabras($cad){
		return ucwords(mb_strtolower($cad));
	}

	private function fixStrangeChars($string){
		$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ¥';
		$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRrñ';
		return strtr($string, utf8_decode($originales) , utf8_decode($modificadas));
	}

	private function createCategoriesIfNeeded($product){
		// @todo - sustituir por setting
		$process_parent_cats = false;

		$categoryIds = [
			'child'=>false,
			'parent'=> false
		];

		if($process_parent_cats){

			$parentName = $product['parent_cat'];
			$name = $product['child_cat'];

			if($parentName !== '' && $name!== ''){

				$categoryIds = $this->getCategoryIdbyName($name, $parentName);

				if(!$categoryIds['parent']){
					$newCat = wp_insert_term($parentName, 'product_cat');

					if(!is_wp_error($newCat)){
						$categoryIds['parent'] = $newCat['term_id'];
						if(!$categoryIds['child']){
							$newCat = wp_insert_term($name, 'product_cat', ['parent'=>$categoryIds['parent']]);

							if(!is_wp_error($newCat)){
								$categoryIds['child'] = $newCat['term_id'];
							}
						}
					}
				}
			}

		}else{
			$parentName = null;
			$name = $product['child_cat'];

			if($name !== ''){
				$categoryIds = $this->getCategoryIdbyName($name, $parentName);

				if(!$categoryIds['child']){
					$newCat = wp_insert_term($name, 'product_cat');

					if(!is_wp_error($newCat)){
						$categoryIds['child'] = $newCat['term_id'];
					}
				}
			}
		}

		return $categoryIds;
	}

	private function getCategoryIdbyName($name, $parentName = null){

		if($parentName){
			$args = array(
		    'name' => $parentName,
		    'parent' => 0,
		    'hide_empty' => false,
		    'fields' => 'ids'
			);
			$terms = get_terms('product_cat', $args);
			if(!empty( $terms ) && !is_wp_error($terms)){
				$parentCatId = $terms[0];
			}else{
				return [
					'parent'=>false,
					'child'=>false
				];
			}
		}

		$args = array(
	    'name' => $name,
	    'hide_empty' => false,
	    'fields' => 'ids'
		);
		if($parentName){
			$args['parent'] = $parentCatId;
		}

		$terms = get_terms('product_cat', $args);
		if(!empty( $terms ) && !is_wp_error($terms)){
			$categoryId = $terms[0];
			return [
				'parent'=>($parentName ? $parentCatId : false),
				'child'=>$categoryId
			];
		}else{
			return [
				'parent'=>($parentName ? $parentCatId : false),
				'child'=>false
			];
		}
	}

	private function getPowershopDBConnection(){

		$db_host=get_option('powershop_db_host');
		$db_nombre=get_option('powershop_db_name');
		$db_user=get_option('powershop_db_user');
		$db_pass=get_option('powershop_db_pass');

		try{

			$connection = new PDO("mysql:host=$db_host;dbname=$db_nombre", $db_user, $db_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return [
				"status" => "ok",
				"conn" => $connection
			];
		}catch(PDOException $e){
			$this->insertLog('error', 'PS Connection', $e->getMessage());
			return [
				"status" => "error",
				"errorMsg" => $e->getMessage(),
				"errorCode" => $e->getCode()
			];
		}
	}

	private function getPowershopProducts($powershop_connection, $settings, $limit = null){

		// Fields needed from powershop
		$powershopNeededFields = [ 'name', 'description', 'price', 'parent_cat', 'child_cat' ];

		$where = apply_filters( 'powershop_get_ps_products_where_clause', '' );

		// Fixed fields
		$powershopColumns[] = 'Codigo';
		$powershopColumns[] = 'Tallas';

		// User defined fields
		foreach ($powershopNeededFields as $field){
			if($settings[$field]!==''){
				$powershopColumns[] = $settings[$field].' '.$field;
			}
		}

		// Check if a Woocommerce attribute is defined for the product brand (and if it is an integer)
		if($settings['brand_attr_id']!=='' && (string)(int)$settings['brand_attr_id'] === $settings['brand_attr_id'] ){
			$powershopColumns[] = 'Marca';
		}

		// Check if a Woocommerce attribute is defined for the product color (and if it is an integer)
		if($settings['color_attr_id']!=='' && (string)(int)$settings['color_attr_id'] === $settings['color_attr_id'] ){
			$powershopColumns[] = 'Color';
		}

		// Select query
		$select = implode(', ', $powershopColumns);

		try{
			$powershop_products_query = $powershop_connection->prepare(
				'SELECT '.$select.'
				FROM PRODUCTOS '.$where.'
				'.($limit ? 'LIMIT '.$limit : '')
			);
			$powershop_products_query->execute();
			return array_map('reset', $powershop_products_query->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC));

		}catch(PDOException $e){
			echo $e->getCode().'<br>';
			echo $e->getMessage();
			return [];
		}

	}

	private function GetAllWooProducts($limit = null){
		global $wpdb;

		return $wpdb->get_results(
			"SELECT pm.meta_value sku, post_title name FROM {$wpdb->prefix}posts p
			LEFT JOIN {$wpdb->prefix}postmeta pm ON (p.ID = pm.post_id)
			WHERE post_type='product'
				AND pm.meta_key = '_sku'
				AND pm.meta_value <> ''
			ORDER BY ID DESC ".($limit ? 'LIMIT '.$limit : ''), OBJECT_K
		);
	}

	private function reindexFacetWP(){
		if(function_exists('FWP')){
			FWP()->indexer->index();
		}
	}

	public function powershopSyncStock(){

		if(!esc_attr(get_option('powershop-cron-stock-active'))){
			return;
		}

		$this->insertLog('notification', 'syncStock', 'Starting stock sync');

		$posts_per_page = 20;
		$initialOffset = 0;

		$info = [
			'posts_per_page'=> $posts_per_page,
			'offset'=> $initialOffset,
			'errors'=> []
		];

		require_once($this->get_path().'/webservices/TCAutoload.php');

		// Init Powershop Web Services
		$wsdl = $this->initPowershopService();

		try{
			$tCServiceWS = new TCServiceWS($wsdl);
		}catch(Exception $e){
			$info['errors']['wsdl'][] = $e->getMessage();
			$this->insertLog('error', 'PS Connection', $e->getMessage());
			$this->sendErrorsEmail($info['errors']);
			return;
		}

		$product_types = $this->getWooProductTypes();

		$sizes_attribute = (int)esc_attr(get_option('powershop_attribute_sizes'));
		$sizes_taxonomy = wc_attribute_taxonomy_name_by_id( $sizes_attribute );
		$tallas = $this->getWooTallas($sizes_taxonomy);
		$talla_unica_name = esc_attr(get_option('powershop_talla_unica'));

		do {
	    $info = $this->syncStock($product_types, $tCServiceWS, $tallas, $info, $talla_unica_name);
		} while ($info['count']);

		$this->sendErrorsEmail($info['errors']);

		//@todo borrar esto
		//echo "<pre>";
		//	print_r($info['errors']);
		//echo "</pre>";
	}


	private function initPowershopService(){
		/*************************
		* Example for TCServiceWS
	 	*/
	 	$wsdl = array();
		$wsdl[TCWsdlClass::WSDL_URL] = esc_attr(get_option('powershop_wsdl_url'));
		$wsdl[TCWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
		return $wsdl;
	}

	private function getWooProductTypes(){
		global $wpdb;
		return $wpdb->get_results(
	 		"SELECT term_id, slug FROM {$wpdb->prefix}terms WHERE slug='simple' OR slug='variable'", OBJECT_K
	 	);
	}


	private function getWooTallas($sizes_taxonomy){
		global $wpdb;
		return $wpdb->get_results(
		 	"SELECT slug, name from wp_terms t
		 	LEFT JOIN wp_term_taxonomy tt ON (t.term_id = tt.term_id)
		 	WHERE taxonomy = '$sizes_taxonomy'", OBJECT_K
		);
	}


	private function syncStock($product_types, $tCServiceWS, $tallas, $info, $talla_unica_name){

		$products = $this->getWooProducts($info['offset']);

		if(count($products)){

			$product_skus = array_keys($products);
			$product_skus_piped = implode('|', $product_skus);
			$productStocksResponse = $this->powershopGetStock($tCServiceWS, $product_skus_piped);

			if($productStocksResponse['status'] == 'error'){
				$info['errors']['wsdl'][] = $productStocksResponse['error'];
				foreach($products as $product){
					$info['errors'][$product->sku][] = __('El producto no se ha podido actualizar por un problema en los WS de powershop', 'powershop');
					$this->insertLog('error', 'syncStock', __('El producto no se ha podido actualizar por un problema en los WS de powershop', 'powershop'));
				}
			}else{
				$productStocks = $productStocksResponse['response'];


				$id_sku_pair = $this->getIdSkuPairs($products);

				$sizes_attribute = (int)esc_attr(get_option('powershop_attribute_sizes'));
				$sizes_taxonomy = wc_attribute_taxonomy_name_by_id( $sizes_attribute );
				$variations = $this->getProductsVariations($products, $product_types, $id_sku_pair, $sizes_taxonomy);

				foreach($products as $product){
					$product_type = $product_types[$product->term_taxonomy_id]->slug;
					if($product_type == 'variable'){
						$info['errors'] = $info['errors'] + $this->updateVariableProductStock($product, $variations, $productStocks, $tallas, $talla_unica_name);
					}elseif($product_type == 'simple') {
						$info['errors'] = $info['errors'] + $this->updateSimpleProductStock($product, $productStocks, $talla_unica_name);
					}
					//echo PHP_EOL . $product_types[$product->term_taxonomy_id]->slug . ' | ' . $product->name.' | id: '.$product->ID .' | stock: ' . $product->stock .' | sku: ' . $product->sku. PHP_EOL;
					//echo "<br>";// debug output
				}

			}

			$info['offset'] = $info['offset'] + $info['posts_per_page'];
		}

		$info['count'] = count($products);

		return $info;
	}


	private function getWooProducts($offset, $post_statuses = ['publish', 'pending']){

		$newStatuses = [];
		foreach ($post_statuses as $status){
			$newStatuses[] = "post_status='".$status."'";
		}
		$status_query = implode(' OR ', $newStatuses);

		global $wpdb;
		// @todo add setting to select post statuses to sync
		return $wpdb->get_results(
			"SELECT pm2.meta_value sku, ID, post_title name, pm.meta_value stock, pm3.meta_value stock_status, tt.term_taxonomy_id FROM {$wpdb->prefix}posts p
			LEFT JOIN {$wpdb->prefix}postmeta pm ON (p.ID = pm.post_id)
			LEFT JOIN {$wpdb->prefix}postmeta pm2 ON (p.ID = pm2.post_id)
			LEFT JOIN {$wpdb->prefix}postmeta pm3 ON (p.ID = pm3.post_id)
			LEFT JOIN {$wpdb->prefix}term_relationships tr ON (p.ID = tr.object_id)
			LEFT JOIN {$wpdb->prefix}term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
			WHERE post_type='product'
				AND (".$status_query.")
				AND pm.meta_key = '_stock'
				AND pm2.meta_key = '_sku'
				AND pm2.meta_value <> ''
				AND pm3.meta_key = '_stock_status'
				AND tt.taxonomy = 'product_type'
			ORDER BY ID DESC
			LIMIT 20
			OFFSET $offset", OBJECT_K
		);
	}

	private function powershopGetStock($tCServiceWS, $product_skus_piped){

		try{
			if(!$tCServiceWS->WS_TO_SeleccionStock($product_skus_piped)){
				$this->insertLog('error', 'syncStock', $tCServiceWS->getLastError());
				return [
					'status' => 'error',
					'error' => $tCServiceWS->getLastError()
				];
			}
		}catch(Exception $e){
			$this->insertLog('error', 'syncStock', $e->getMessage());
			return [
				'status' => 'error',
				'error' => $e->getMessage()
			];
		}

		$WsStockResult = $tCServiceWS->getResult();

		$formattedStocks = [];
		$numberOfTallas = 17;

		foreach ( $WsStockResult['Salida1'] as $key => $sku ){
			for ($i = 3; $i <= 19; $i++ ){
				$talla = 'Salida' . ($numberOfTallas + $i);
				$stock = 'Salida' . $i;
				if($WsStockResult[$talla][$key] !== ''){
					$formattedStocks[$sku][$WsStockResult[$talla][$key]] = $WsStockResult[$stock][$key];
				}
			}
		}

		return [
			'status' => 'ok',
			'response' => $formattedStocks
		];
	}

	private function getIdSkuPairs($products){
		$id_sku_pair = [];
		foreach ($products as $product){
			$id_sku_pair[$product->ID] = 	$product->sku;
		}
		return $id_sku_pair;
	}


	private function getProductsVariations($products, $product_types, $id_sku_pair, $sizes_taxonomy){

		global $wpdb;

		$variable_product_ids = $this->getVariableProductIds($products, $product_types);
		$temp_variations = [];

		if(!empty($variable_product_ids)){
			$post_parents = implode(',',$variable_product_ids);

			$temp_variations = $wpdb->get_results(
				"SELECT ID, post_parent, pm.meta_value stock, pm2.meta_value talla, pm3.meta_value stock_status FROM {$wpdb->prefix}posts p
				LEFT JOIN {$wpdb->prefix}postmeta pm ON (p.ID = pm.post_id)
				LEFT JOIN {$wpdb->prefix}postmeta pm2 ON (p.ID = pm2.post_id)
				LEFT JOIN {$wpdb->prefix}postmeta pm3 ON (p.ID = pm3.post_id)
				WHERE post_type='product_variation'
					AND post_parent IN ($post_parents)
					AND (post_status='publish' OR post_status='draft')
					AND pm.meta_key = '_stock'
					AND pm2.meta_key = 'attribute_$sizes_taxonomy'
					AND pm3.meta_key = '_stock_status'",
				OBJECT
			);
		}

		if(count($temp_variations)){
			$variations = [];
			foreach ($temp_variations as $v){
				$sku = $id_sku_pair[$v->post_parent];
				//echo PHP_EOL.$v->post_parent.'='.$sku.PHP_EOL;
				$variations[$sku][$v->talla] = $v;
			}
			return $variations;
		}else{
			return [];
		}
	}

	private function getVariableProductIds($products, $product_types){
		$variable_product_id = [];
		foreach($products as $product){
			if($product_types[$product->term_taxonomy_id]->slug == 'variable'){
				$variable_product_id[] = $product->ID;
			}
		}
		return $variable_product_id;
	}


	private function updateVariableProductStock($product, $variations, $productStocks, $tallas, $talla_unica_name){

		$errors = [];

		if(!isset($productStocks[$product->sku])){
			$message = __('El producto no existe en PS', 'powershop');
			$errors[$product->sku][] = $message;
			$this->insertLog('error', 'syncStock', $message, $product->sku);
		}else{

			$productWoo = $variations[$product->sku];
			$productPowershop = $productStocks[$product->sku];

			// Eliminamos la talla "UNICA" porque no debería estar en los productos variable
			// aunque a veces nos llegan las tallas con algo parecido a XXS|XS|S|M|L|XL|XXL|XS-S|S-M|M-L|L-XL||||||UNICA|
			unset($productPowershop[$talla_unica_name]);

			if(count($productWoo)!=count($productPowershop)){
				$message = 'Número de tallas discrepa entre Woo ('.count($productWoo).') y PS ('.count($productPowershop).')';
				$errors[$product->sku][] = $message;
				$this->insertLog('error', 'syncStock', $message, $product->sku);
			}

			$stock_status = 'outofstock';

			foreach ($productWoo as $talla){
				$tallaName = $tallas[$talla->talla]->name;

				// stock_status value for parent product
				if($stock_status == 'outofstock' && intval($talla->stock) > 0){
					$stock_status = 'instock';
				}

				// La talla no existe en powershop
				if(!isset($productPowershop[$tallaName])){
					$errors[$product->sku][] = 'La talla ' . $tallaName. ' no existe en PS';
					continue;
				}

				$stockTallaPowershop = $productPowershop[$tallaName];

				// Si el stock difiere en powershop y woocommerce actualizamos el stock
				if(intval($stockTallaPowershop) != intval($talla->stock)){
					$message = 'El stock de la talla '. $tallaName . ' es distinta en PS ('.intval($stockTallaPowershop).') y Woo ('.intval($talla->stock).')';
					$this->insertLog('notification', 'syncStock', $message, $product->sku);
					wc_update_product_stock( $talla->ID, intval($stockTallaPowershop));

				// Si el stock de powershop para esta talla coincide con el stock en powershop no actualizamos stock
				}else{
					//$message = "No hay que actualizar, stock de ".intval($productPowershop[$tallaName])." en Woo y PS";
					//$this->insertLog('notification', 'syncStock', $message, $product->sku);
				}

				// Set correct stock status to variation
				$talla_stock_status = intval($stockTallaPowershop) > 0 ? 'instock' : 'outofstock';
				if($talla_stock_status !== $talla->stock_status){
					$message = 'El status del stock de la talla '. $tallaName . ' ha cambiado a '.$talla_stock_status;
					$this->insertLog('notification', 'syncStock', $message, $product->sku);
					wc_update_product_stock_status( $talla->ID, $talla_stock_status );
				}

				unset($productPowershop[$tallaName]);
				
			}

			// Set correct stock status to parent product
			if($stock_status !== $product->stock_status){
				$message = 'stock_status for  '.$product->ID.' updated to '.$stock_status;
				$this->insertLog('notification', 'syncStock', $message, $product->sku);
				wc_update_product_stock_status( $product->ID, $stock_status);
			}

			if(count($productPowershop)){
				$message = 'Hay '.count($productPowershop).' tallas en PS que no existen en Woo';
				$errors[$product->sku][] = $message;
				$this->insertLog('error', 'syncStock', $message, $product->sku);
			}

		}
		return $errors;
	}


	private function updateSimpleProductStock($product, $productStocks, $talla_unica_name){

		$errors = [];

		if(!isset($productStocks[$product->sku])){
			$message = __('El producto no existe en PS', 'powershop');
			$errors[$product->sku][] = $message;
			$this->insertLog('error', 'syncStock', $message, $product->sku);
		}else{
			
			$productPowershop = $productStocks[$product->sku];
			if(!isset($productPowershop[$talla_unica_name])){
				$message = __('Es un producto simple en Woo pero no tiene definida la "talla única "'.$talla_unica_name.' en PS', 'powershop');
				$errors[$product->sku][] = $message;
				$this->insertLog('error', 'syncStock', $message, $product->sku);
			}else{
				
				if(intval($productPowershop[$talla_unica_name]) !== intval($product->stock)){
					$message = __('El stock es distinto en Woo ('.intval($product->stock).') y PS ('.intval($productPowershop[$talla_unica_name]).')', 'powershop');
					$this->insertLog('notification', 'syncStock', $message, $product->sku);
					wc_update_product_stock( $product->ID, $productPowershop[$talla_unica_name]);
				}

				$stock_status = intval($productPowershop[$talla_unica_name]) > 0 ? 'instock' : 'outofstock';

				// Set correct stock status to parent product
				if ( $stock_status !== $product->stock_status ) {
					$message = 'stock_status for  '.$product->ID.' updated to '.$stock_status;
					echo $message.'<br>';
					$this->insertLog('notification', 'syncStock', $message, $product->sku);
					wc_update_product_stock_status( $product->ID, $stock_status);
				}
			}
		}

		return $errors;

	}

	// create a scheduled event (if it does not exist already)
	public function powershopCronJobs() {
		if(!wp_next_scheduled('powershop_sync_stock')){
		   wp_schedule_event( time(), 'powershop_stock', 'powershop_sync_stock' );
		   $this->write_log('activate sync stock cron');
		}
		if(!wp_next_scheduled('powershop_sync_orders')){
		   wp_schedule_event( time(), 'powershop_orders', 'powershop_sync_orders' );
		   $this->write_log('activate sync orders cron');
		}
	}


	public function powershop_cron_schedules( $schedules ) {
		$default_interval_minutes = 20;
		$interval_minutes = intval(esc_attr( get_option('powershop-cron-stock-mins') ));
		$interval_minutes = is_int($interval_minutes) && $interval_minutes > 0 ? $interval_minutes : $default_interval_minutes;
		$interval_secs = $interval_minutes * 60;

	  $schedules['powershop_stock'] = array(
	    'interval' => $interval_secs,
	    'display' => sprintf(__('Once every %s minutes', 'powershop'), $interval_minutes)
	  );

	  $default_interval_minutes = 10;
	  $interval_minutes = intval(esc_attr( get_option('powershop-cron-orders-mins') ));
	  $interval_minutes = is_int($interval_minutes) && $interval_minutes > 0 ? $interval_minutes : $default_interval_minutes;
	  $interval_secs = $interval_minutes * 60;

	  $schedules['powershop_orders'] = array(
	    'interval' => $interval_secs,
	    'display' => sprintf(__('Once every %s minutes', 'powershop'), $interval_minutes)
	  );

	  return $schedules;
	}


	private function write_log ( $log )  {
	  if ( true === WP_DEBUG ) {
	    if ( is_array( $log ) || is_object( $log ) ) {
	    	error_log( print_r( $log, true ) );
	    } else {
	    	error_log( $log );
	    }
	  }
  }

  private function sendErrorsEmail($stockErrors){

		$message = '';

		if(!empty($stockErrors)){

			foreach ($stockErrors as $sku => $errors){
				$message.= $sku.'<br>';
				for ($i = 1; $i <= strlen ($sku); $i++ ){
					$message.= '_';
				}
				$message.= '<br><br>';
				foreach ($errors as $error){
					$message.= $error.'<br>';
				}
				$message.= '<br><br><br>';
			}

			$this->sendNotification('Errores de sync de stock', $message);

		}
	}

	private function sendNotification($type, $message){

		$asunto = get_bloginfo('name').' - '.$type.' from site: '.get_bloginfo('url');
		$headers = array(
			"MIME-Version: 1.0",
			"Content-Type: text/html; charset=UTF-8"
		);

		$toEmails = esc_attr(get_option('powershop-notifications-emails'));
		if(!$toEmails){
			$toEmails = [get_option('admin_email')];
		}else{
			$toEmails = str_ireplace(' ', '', $toEmails);
			$toEmails = explode(',', $toEmails);
		}

		$send=wp_mail($toEmails, $asunto, $message, $headers);
	}


	public function addOrderForSync($order_id){

		$this->insertLog('notification', 'syncOrders', 'Adding order to sync queue', $order_id);
		$order = wc_get_order( $order_id );
    $order->update_meta_data( '_powershop_needs_process', 1 );
    $order->save();

	}

	public function powershopCustomOrderQueryVars( $query, $query_vars ) {

		if ( isset( $query_vars['_powershop_needs_process'] ) ) {
			$query['meta_query'][] = array(
				'key' => '_powershop_needs_process',
				'value' => esc_attr( $query_vars['_powershop_needs_process'] ),
			);
		}

		if ( isset( $query_vars['_powershop_processed'] ) ) {

			// if query_var value = 0 return results where meta does not exist or is = 0
			if(!$query_vars['_powershop_processed']){
				$query['meta_query'][] = array(
					'relation' => 'OR',
					array(
						'key'     => '_powershop_processed',
						'value'   => 0,
						'compare' => 'NOT EXISTS'
					),
					array(
						'key'     => '_powershop_processed',
						'value'   => '0',
						'compare' => '='
					)
				);
			}else{
				$query['meta_query'][] = array(
					'key' => '_powershop_processed',
					'value' => esc_attr( $query_vars['_powershop_processed'] ),
				);
			}
		}
		return $query;
	}

	public function powershopSyncOrders(){

		$test_mode = esc_attr(get_option('powershop_test_active'));

		// This is a safety measure to prevent sync of testing environments with powershop
		if(esc_attr(get_option('powershop_shop_url')) !== get_bloginfo('url') &&  !$test_mode ){
			$this->insertLog('error', 'syncOrders', __('La URL de la tienda no encaja con la especificada en las opciones de Powershop', 'powershop'));
			return;
		}

		if($test_mode){
			$test_codes = esc_attr(get_option('powershop_test_codes'));
			if($test_codes){
				$test_codes = explode(',', str_ireplace(' ', '', $test_codes));
			}else{
				$test_codes = [];
			}
		}

		if(!esc_attr(get_option('powershop-cron-orders-active'))){
			return;
		}

		// Get limit for number of orders to sync
		$limit = esc_attr( get_option('powershop-cron-orders-limit') );
		$limit = $limit ? $limit : 10;

		$orders = wc_get_orders(
			array(
				'limit' => $limit,
				'_powershop_needs_process' => 1,
				'_powershop_processed' => 0
			)
		);

		// Return if no pending orders
		if(!count($orders)){
			return;
		}

		$talla_unica_name = esc_attr( get_option('powershop_talla_unica'));
		if(!$talla_unica_name){
			$message = __('No se ha especificado el nombre de las talla Única de Powershop', 'powershop');
			$this->insertLog('error', 'syncOrders', $message, $order_id);
			$this->sendNotification($message);
			return;
		}

		require_once($this->get_path().'/webservices/TCAutoload.php');

		// Init Powershop Web Services
		$wsdl = $this->initPowershopService();

		try{
			$tCServiceWS = new TCServiceWS($wsdl);
		}catch(Exception $e){
			$info['errors']['wsdl'][] = $e->getMessage();
			$this->insertLog('error', 'PS Connection', $e->getMessage());
			$this->sendErrorsEmail($info['errors']);
			return;
		}

		// @Todo ver si es más eficiente usar tallas_index que consultar el nombre de la talla a partir del slug con get_term_by más abajo
		//$tallas_index = $this->getWooTallas($sizes_taxonomy);

		$sizes_taxonomy = null;

		foreach($orders as $order){

			$this->insertLog('notification', 'syncOrders', 'Starting sync of order with PS', $order->get_id());

			$order_data = $order->get_data();

			$client_name = $order_data['billing']['first_name'].' '.$order_data['billing']['last_name'];

			$skus = [];
			$quantities = [];
			$prices = [];
			$tallas = [];

			foreach ($order->get_items() as $item_id => $item_data) {

				$product = new WC_Product($item_data->get_product_id());

				// If in test mode, only allow sending of test products to powershop
				if($test_mode && !in_array($product->get_sku(), $test_codes)){
					$this->insertLog('notification', 'syncOrders', 'No se puede enviar el código '.$product->get_sku().' a PS en modo test', $order->get_id());
					continue;
				}

				if($item_data->get_variation_id()){

					if(!$sizes_taxonomy){
						$sizes_attribute = (int)esc_attr(get_option('powershop_attribute_sizes'));
						$sizes_taxonomy = wc_attribute_taxonomy_name_by_id( $sizes_attribute );
					}

					$talla_slug = wc_get_order_item_meta($item_data->get_id(), $sizes_taxonomy, true);
					$talla = get_term_by('slug', $talla_slug, $sizes_taxonomy);
					$line_talla = $talla->name;

				}else{

					$line_talla = $talla_unica_name;
				}



				$tallas[] = $line_talla;
				$skus[] = $product->get_sku();
				$quantities[] = $item_data->get_quantity();

				$line_price = number_format($item_data->get_total()/$item_data->get_quantity(), 2);
				$prices[] = $line_price;

				$message = 'Adding order item | sku: '.$product->get_sku().' | qty: '.$item_data->get_quantity().' | size: '.$line_talla.' | price: '.$line_price;
				$this->insertLog('notification', 'syncOrders', $message, $order->get_id());

			}

			//@todo - quitar estas salidas que son para testing
			//echo "<pre>";
			// print_r($skus);
			// print_r($tallas);
			// print_r($quantities);
			// print_r($prices);
			//echo "</pre>";
			//die();

			if(!empty($skus)){

				if( $tCServiceWS->WS_TO_GrabarPedidoCliente(
					0,																				// $Entrada1 -> RegCliente (Int) Código de cliente de PowerShop
					false,																		// $Entrada2 -> Actualizar (Bool) True si el cliente existe y se quieren actualizar los datos. False si el cliente es nuevo.
					0,																				// $Entrada3 -> Multiweb (Int) 0 cuando no está configurada la opción en PowerShop
					null,																			// $Entrada4 -> DNI (Str) Documento de identidad del cliente
					null,																			// $Entrada5 -> Usuario (Str) Nombre de usuario del cliente
					null,																			// $Entrada6 -> Password (Str) Contraseña del cliente
					$client_name,															// $Entrada7 -> Nombre (Str) Nombre del cliente
					'',																				// $Entrada8 -> Contacto (Str) Contacto (si hubiera diferentes direcciones de envío)
					'minorista',															// $Entrada9 -> TipoCliente (Str) Tipo de cliente, siempre será minorista
					false,																		// $Entrada10 -> Factura (Bool) Emisión de factura. Si está a true se deberá comprobar que se envían los datos necesarios
					$order_data['payment_method_title'],			// $Entrada11 -> FormaPago (Str) Forma de pago
					null,																			// $Entrada12 -> Aceptacion (Str) Código de aceptación de pago (TPV o PayPal)
					'',																				// $Entrada13 -> Banco (Str) Nombre del banco en caso de cargo en cuenta
					'', 																			// $Entrada14 -> Agencia (Str) Agencia de la cuenta
					'', 																			// $Entrada15 -> DC_IBAN (Str) Código del IBAN de la cuenta
					'',																				// $Entrada16 -> DC_Entidad (Str) Código de entidad de la cuenta
					'',																				// $Entrada17 -> DC_Agencia (Str) Agencia de la cuenta
					'',																				// $Entrada18 -> DC_Control (Str) Código de control del número de cuenta
					'',																				// $Entrada19 -> DC_Cuenta (Str) Número de cuenta
					$order_data['billing']['address_1'],			// $Entrada20 -> Direccion (Str) Dirección de facturación
					$order_data['billing']['city'],						// $Entrada21 -> Localidad (Str) Localidad de la dirección de facturación
					$order_data['billing']['state'],					// $Entrada22 -> Provincia (Str) Provincia de la dirección de facturación
					$order_data['billing']['postcode'],				// $Entrada23 -> Código (Str) Código postal de la dirección de facturación
					$order_data['billing']['country'],				// $Entrada24 -> Pais (Str) País de la dirección de facturación
					$order_data['shipping']['address_1'],			// $Entrada25 -> EDireccion (Str) Dirección de envío del pedido
					$order_data['shipping']['city'],					// $Entrada26 -> ELocalidad (Str) Localidad de la dirección en envío
					$order_data['shipping']['state'],					// $Entrada27 -> EProvincia (Str) Provincia de la dirección en envío
					$order_data['shipping']['postcode'],			// $Entrada28 -> EPostal (Str) Código postal de la dirección de envío
					$order_data['shipping']['country'],				// $Entrada29 -> EPais (Str) País de la dirección de envío
					$order_data['billing']['address_1'],			// $Entrada30 -> FDireccion (Str) Dirección de envío de factura
					$order_data['billing']['city'],						// $Entrada31 -> FLocalidad (Str) Localidad de la dirección de envío de la factura
					$order_data['billing']['state'],					// $Entrada32 -> FProvincia (Str) Provincia de la dirección de envío de la factura
					$order_data['billing']['postcode'],				// $Entrada33 -> FPostal (Str) Código postal de la dirección de envío de la factura
					$order_data['billing']['country'],				// $Entrada34 -> FPais (Str) País
					$order_data['billing']['phone'],					// $Entrada35 -> Telefono1 (Str) Número de teléfono del cliente
					'',																				// $Entrada36 -> Telefono2 (Str) Número de teléfono del cliente
					'',																				// $Entrada37 -> Fax (Str) Número de Fax del cliente
					'',																				// $Entrada38 -> Movil (Str) Número de teléfono móvil del cliente
					'',																				// $Entrada39 -> Web (Str) Url de la web del cliente
					$order_data['billing']['email'],					// $Entrada40 -> Email (Str) Dirección de correo electrónico del cliente
					'',																				// $Entrada41 -> Marketing1 (Str) Valores de texto para uso específico del comercio.
					'',																				// $Entrada42 -> Marketing2.
					'',																				// $Entrada43 -> Marketing3.
					'',																				// $Entrada44 -> Marketing4.
					'',																				// $Entrada45 -> Marketing5.
					'',																				// $Entrada46 -> Marketing6.
					'',																				// $Entrada47 -> Marketing7.
					false,																		// $Entrada48 -> Sexo (Str) Sexo del cliente, admite los valores "Hombre" o "Mujer"
					'',																				// $Entrada49 -> Nacimiento[Date] Fecha de nacimiento del cliente en formato (YYYY-MM-DD)
					false,																		// $Entrada50 -> RecibirInfo (Bool) Acepta recibir información
					'',																				// $Entrada51 -> Interesado (Str) Temas o tipos de producto que interesan al cliente
					'',																				// $Entrada52 -> ComentariosC (Str) Para información o datos que no tengan otro lugar
					$order->get_id(),													// $Entrada53 -> IdPedido (Str) Número de pedido generado por la plataforma de eCommerce
					$order_data['customer_note'],							// $Entrada54 -> Comentarios (Str) Comentarios adicionales o especiales sobre el pedido
					'',																				// $Entrada55 -> Transportista (Str) Denominación del transportista
					$order_data['shipping_total'],						// $Entrada56 -> Gastos (Str) Gastos de preparación, manipulación, transporte, etc.
					false,																		// $Entrada57 -> Regalo (Bool) Si el pedido es un regalo. Se podría generar un ticket regalo en PowerShop
					false,																		// $Entrada58 -> Canarias (Bool) Si el envío es para Canarias. Podrían cambiar los impuestos aplicados
					'',																				// $Entrada59 -> Comercial (Str) Nombre comercial del cliente
					join('|', $skus),													// $Entrada60 -> Códigos (Str) Códigos de los artículos del pedido separados por pipes "|"
					join('|', $tallas),												// $Entrada61 -> Tallas (Str) Nombres de las tallas de los artículos separados por pipes "|"
					join('|', $quantities),										// $Entrada62 -> Unidades (Str) Cantidad de los artículos separados por pipes "|"
					join('|', $prices)												// $Entrada63 -> Precios (Str) Precios de los artículos separados por pipes "|"
				)){
					$this->insertLog('notification', 'syncOrders', $tCServiceWS->getResult(), $order->get_id());
					//print_r($tCServiceWS->getResult());
				}else{
					$this->insertLog('error', 'syncOrders', $tCServiceWS->getLastError(), $order->get_id());
					//print_r($tCServiceWS->getLastError());
				}
			}

			//@todo descomentar esto para que se marque el pedido como procesado
			$order->update_meta_data( '_powershop_processed', 1 );
			$order->save();

		}
	}

	public function insertLog($type, $context, $message, $identifier = null){

		global $wpdb;

		$table_name = $wpdb->prefix . 'powershop_logs';

		if ( is_array( $message ) || is_object( $message ) ) {
    	$message = json_encode($message);
    }

		$wpdb->insert(
			$table_name,
			array(
				'time' => current_time( 'mysql' ),
				'type' => $type,
				'context' => $context,
				'message' => $message,
				'identifier' => $identifier,
			)
		);
	}

	// Removes manage stock at product level for variable products
	public function removeManageStockForVariable(){

		$product_types = $this->getWooProductTypes();

		global $wpdb;
		// @todo add setting to select post statuses to sync
		$variableProducts = $wpdb->get_results(
			"SELECT pm2.meta_value sku, ID, post_title name, t.slug FROM {$wpdb->prefix}posts p
			LEFT JOIN {$wpdb->prefix}postmeta pm2 ON (p.ID = pm2.post_id)
			LEFT JOIN {$wpdb->prefix}term_relationships tr ON (p.ID = tr.object_id)
			LEFT JOIN {$wpdb->prefix}term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
			LEFT JOIN {$wpdb->prefix}terms t ON (tt.term_id = t.term_id)

			WHERE post_type='product'
				AND pm2.meta_key = '_sku'
				AND pm2.meta_value <> ''
				AND tt.taxonomy = 'product_type'
				AND t.slug = 'variable'
			ORDER BY ID DESC", OBJECT_K
		);

		$sizes_attribute = (int)esc_attr(get_option('powershop_attribute_sizes'));
		$sizes_taxonomy = wc_attribute_taxonomy_name_by_id( $sizes_attribute );

		foreach ($variableProducts as $product) {
			$variations = $wpdb->get_results(
				"SELECT post_parent, pm.meta_value stock, pm2.meta_value talla FROM {$wpdb->prefix}posts p
				LEFT JOIN {$wpdb->prefix}postmeta pm ON (p.ID = pm.post_id)
				LEFT JOIN {$wpdb->prefix}postmeta pm2 ON (p.ID = pm2.post_id)
				WHERE post_type='product_variation'
					AND post_parent = ".$product->ID."
					AND pm.meta_key = '_stock'
					AND pm2.meta_key = 'attribute_$sizes_taxonomy'",
				OBJECT
			);
			echo PHP_EOL . $product->slug. ' | ' . $product->name.' | id: '.$product->ID .' | stock: ' . $product->stock .' | sku: ' . $product->sku. PHP_EOL;
			echo "<br>";// debug output
			$stock = 0;
			foreach ($variations as $variation){
				echo PHP_EOL . 'talla: '. $variation->talla. ' | stock: ' . $variation->stock.' | parent: '.$variation->post_parent . PHP_EOL;
				echo "<br>";
				$stock += $variation->stock;
			}
			$stock_status = ($stock > 0 ? 'instock':'outofstock');
			update_post_meta( $product->ID, '_manage_stock', "no" );
			update_post_meta( $product->ID, '_stock', null );
			update_post_meta( $product->ID, '_stock_status', $stock_status );

			echo "variation stock:".$stock;
			echo "<br>";
			echo "stock status:".$stock_status;
			echo "<br>";
			echo "<br>";
		}
	}

}
