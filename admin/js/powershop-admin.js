(function( $ ) {
	'use strict';

	$( document ).ready(function() {

		if($('#new-powershop-products').length){

			var productCount = parseInt($('.product-count').data('count'));
			var importedNumber = 0;
			var percentage = 0;
			var importLog = $('#import-log-inner');
			var importing = false;
			var messagesWrapper = $('#powershop-import-products-content .import-messages');

	    var importProductsBtn = $('#powershop-import-products-content #import-products-btn');

	    importProductsBtn.on('click', function(){

		    if(importing){
			    return;
		    }
		    importing = true;

				var newProducts = JSON.parse(document.getElementById('new-powershop-products').innerHTML);
				var products = [];

				$.each(newProducts, function( sku, product ) {
					var p = product;
					p.sku = sku;
					products.push(p);
				});

		    if(products.length){
					importProduct(products);
		    }
	    });


	    function importProduct(products){

		    var data = {
					'action': 'import_powershop_product',
					'sku': products[importedNumber].sku,
					'product': products[importedNumber],
					'dataType': 'json',
				};

				$.ajax({
				  type: "POST",
				  url: powershop.ajax_url,
				  data: data,
				  success: function(response){

				 		var message = response.status == 'ok' ? 'Importado correctamente' : response.error.type;
				 		importLog.prepend( '<li class="'+(response.status=='ok'?'good':'error')+'">' + products[importedNumber].sku + ' ' + products[importedNumber].name + ' ' + message + '</li>' );
				 		if(response.status=='error'){
					 		console.log(response.error);
				 		}
				 		importedNumber++;
				 		percentage = parseInt(importedNumber/productCount*100)+'%';

				 		$('.progress-bar-inner').css('width', percentage);
				 		$('.progress-bar-inner').html(percentage);

				 		setTimeout(function(){
						  if(importedNumber < productCount){
					 			importProduct(products);
				 			}else{
					 			afterImport();
				 			}
						}, 200);
				  }
				});
			}

			function afterImport(){

				messagesWrapper.html('Productos importados.  Ahora tenermos que re-indexar.  No cierres esta ventana!!!');
				messagesWrapper.css('color', 'red');

		    var data = {
					'action': 'after_import_powershop_products',
				};
				$.ajax({
				  type: "POST",
				  url: powershop.ajax_url,
				  data: data,
				  dataType: 'json',
				  success: function(response){
						if(response.status == 'ok'){
							messagesWrapper.html('Re-indexación completa!!! Ya puedes cerrar la ventana');
							messagesWrapper.css('color', 'green');
						}
				  }
				});
			}

    }


    $('#powershop-search-logs').on('click', function(){

	    /*
			 * queryParameters -> handles the query string parameters
			 * queryString -> the query string without the fist '?' character
			 * re -> the regular expression
			 * m -> holds the string matching the regular expression
			 */
			var queryParameters = {}, queryString = location.search.substring(1),
			    re = /([^&=]+)=([^&]*)/g, m;

			// Creates a map with the query string parameters
			while (m = re.exec(queryString)) {
			    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
			}


			// Add new parameters or update existing ones
			var indentifier = $('#powershop-logs-identifier').val();
			if(indentifier){
				queryParameters['log_identifier'] = indentifier;
			}else{
				queryParameters['log_identifier'] = '';
			}

			var message = $('#powershop-logs-msg').val();
			if(message){
				queryParameters['log_message'] = message;
			}else{
				queryParameters['log_message'] = '';
			}

			var context = $('#powershop-logs-context').val();
			if(context){
				queryParameters['log_context'] = context;
			}else{
				queryParameters['log_context'] = '';
			}

			var type = $('#powershop-logs-type').val();
			if(type){
				queryParameters['log_type'] = type;
			}else{
				queryParameters['log_type'] = '';
			}

			//queryParameters['existingParameter'] = 'new value';

			/*
			 * Replace the query portion of the URL.
			 * jQuery.param() -> create a serialized representation of an array or
			 *     object, suitable for use in a URL query string or Ajax request.
			 */
			location.search = $.param(queryParameters); // Causes page to reload



    });

	});

})( jQuery );
