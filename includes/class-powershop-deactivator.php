<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://peanutbutter.es
 * @since      1.0.0
 *
 * @package    Powershop
 * @subpackage Powershop/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Powershop
 * @subpackage Powershop/includes
 * @author     Pablo Giralt <pablo@peanutbutter.es>
 */
class Powershop_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		error_log('deactivate my cron');
		// find out when the last event was scheduled
		$timestamp = wp_next_scheduled ('powershop_sync_stock');
		// unschedule previous event if any
		wp_unschedule_event ($timestamp, 'powershop_sync_stock');

		// find out when the last event was scheduled
		$timestamp = wp_next_scheduled ('powershop_sync_orders');
		// unschedule previous event if any
		wp_unschedule_event ($timestamp, 'powershop_sync_orders');
	}

	private function write_log ( $log )  {
	  if ( true === WP_DEBUG ) {
	    if ( is_array( $log ) || is_object( $log ) ) {
	    	error_log( print_r( $log, true ) );
	    } else {
	    	error_log( $log );
	    }
	  }
  }

}
