<?php

/**
 * Fired during plugin activation
 *
 * @link       http://peanutbutter.es
 * @since      1.0.0
 *
 * @package    Powershop
 * @subpackage Powershop/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Powershop
 * @subpackage Powershop/includes
 * @author     Pablo Giralt <pablo@peanutbutter.es>
 */
class Powershop_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Create Powershop Log Table
		$powershopActivator = new Powershop_Activator();
		$powershopActivator->createLogsTable();
	}

	/*
		Note that the dbDelta function is rather picky, however. For instance:

		- You must put each field on its own line in your SQL statement.
		- You must have two spaces between the words PRIMARY KEY and the definition of your primary key.
		- You must use the key word KEY rather than its synonym INDEX and you must include at least one KEY.
		- KEY must be followed by a SINGLE SPACE then the key name then a space then open parenthesis with the field name then a closed parenthesis.
		- You must not use any apostrophes or backticks around field names.
		- Field types must be all lowercase.
		- SQL keywords, like CREATE TABLE and UPDATE, must be uppercase.
		- You must specify the length of all fields that accept a length parameter. int(11), for example.
	*/

	private function createLogsTable() {
		//error_log('Create log table');

		global $wpdb;

		$powershop_db_version = '1.0';

		$table_name = $wpdb->prefix . 'powershop_logs';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			type tinytext NOT NULL,
			context tinytext NOT NULL,
			message text NOT NULL,
			identifier tinytext,
			PRIMARY KEY  (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		add_option( 'powershop_db_version', $powershop_db_version );
	}
}
